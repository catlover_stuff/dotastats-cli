import https from 'node:https'
import http from 'node:http'
import fs from 'node:fs'

import {
    ConvertedLaneOutcome,
    GameplayStats,
    LaneOutcome,
    ReplayIdData,
    StratzFetchResult,
    StratzGamePlayer,
    StratzMatch,
    StratzPlayerResponse,
} from './types'
import { averageimp, winrate } from './calculation-helpers'

import { getData, getDataFromFile, saveCache } from './cacheHandling'
import { B, G, NC, R } from './display/colors'
import { get, head, post } from './request-utils'
import { config } from './constants'
import {
    convertDate,
    getCurrentPlayer,
    getLaneOutcomes,
    isParsed,
} from './data-interface'
import { expandWithSpaces } from './display/utils/print-utils'

export const STATS_KEYS = {
    winrate: 'winrate',
    lanewinrate: 'lanewinrate',
    imp: 'imp',
} as const

type ToString<T> = T extends string | number | boolean | { length: number }
    ? string
    : PropertiesToString<T>

type PropertiesToString<T> = {
    [K in keyof T]: ToString<T[K]>
}

// FIXME: array in type
const matchGraphqlQuery: any /*PropertiesToString<StratzMatch>*/ = {
    parsedDateTime: 'parsedDateTime',
    startDateTime: 'startDateTime',
    id: 'id',
    winRates: 'winRates',
    radiantNetworthLeads: 'radiantNetworthLeads',
    radiantExperienceLeads: 'radiantExperienceLeads',
    didRadiantWin: 'didRadiantWin',
    direKills: 'direKills',
    radiantKills: 'radiantKills',
    regionId: 'regionId',
    predictedWinRates: 'predictedWinRates',
    topLaneOutcome: 'topLaneOutcome',
    midLaneOutcome: 'midLaneOutcome',
    bottomLaneOutcome: 'bottomLaneOutcome',
    rank: 'rank',
    analysisOutcome: 'analysisOutcome',
    gameMode: 'gameMode',
    lobbyType: 'lobbyType',
    durationSeconds: 'durationSeconds',
    players: {
        kills: 'kills',
        deaths: 'deaths',
        assists: 'assists',
        networth: 'networth',
        level: 'level',
        numLastHits: 'numLastHits',
        numDenies: 'numDenies',
        steamAccountId: 'steamAccountId',
        steamAccount: {
            name: 'name',
            seasonRank: 'seasonRank',
        },
        stats: {
            itemPurchases: {
                time: 'time',
                itemId: 'itemId',
            },
            level: 'level',
            lastHitsPerMinute: 'lastHitsPerMinute',
            deniesPerMinute: 'deniesPerMinute',
            networthPerMinute: 'networthPerMinute',
            experiencePerMinute: 'experiencePerMinute',
            goldPerMinute: 'goldPerMinute',
            heroDamagePerMinute: 'heroDamagePerMinute',
            heroDamageReceivedPerMinute: 'heroDamageReceivedPerMinute',
            healPerMinute: 'healPerMinute',
            campStack: 'campStack',
            allTalks: {
                message: 'message',
                time: 'time',
            },
        },
        isVictory: 'isVictory',
        isRadiant: 'isRadiant',
        partyId: 'partyId',
        lane: 'lane',
        role: 'role',
        position: 'position',
        hero: {
            displayName: 'displayName',
        },
        imp: 'imp',
        award: 'award',
        backpack0Id: 'backpack0Id',
        backpack1Id: 'backpack1Id',
        backpack2Id: 'backpack2Id',
        item0Id: 'item0Id',
        item1Id: 'item1Id',
        item2Id: 'item2Id',
        item3Id: 'item3Id',
        item4Id: 'item4Id',
        item5Id: 'item5Id',
        neutral0Id: 'neutral0Id',
        playerSlot: 'playerSlot',
    },
}

const getMatchGraphqlQuery = (
    obj: { [key: string]: any } = matchGraphqlQuery,
    depth = 1
) => {
    const prefix = expandWithSpaces('', depth * 3)
    const query = Object.entries(obj).reduce((acc, [key, value]) => {
        if (typeof value === 'object') {
            return (
                acc +
                `${prefix}${key} ` +
                getMatchGraphqlQuery(value, depth + 1)
            )
        } else {
            return acc + `${prefix}${key}\n`
        }
    }, '')

    const bracketPrefix = expandWithSpaces('', (depth > 0 ? depth - 1 : 0) * 3)
    return `{\n` + query + `${bracketPrefix}}\n`
}

const postToStratzGraphql = <T>(data: string) => {
    return post<T>('api.stratz.com', `/graphql`, data, {
        Authorization: `Bearer ${config.STRATZ_API_KEY}`,
    })
}

export const requestParse = async (matchId) => {
    const data = `{"operationName":"NotDidRequestDownloadAlertMutation","variables":{"matchId":${matchId}},"query":"mutation NotDidRequestDownloadAlertMutation($matchId: Long!) {retryMatchDownload(matchId: $matchId)}"}`
    const response = await postToStratzGraphql<{
        data: { retryMatchDownload: boolean }
    }>(data)

    if (response.data.retryMatchDownload === true) {
        console.log(`${G}Parsing for ${B}${matchId}${G} requested.${NC}`)
    } else {
        console.log(`${R}Something went wrong${NC}`)
        console.log(JSON.stringify(response, null, 2))
    }
}

const fetchMatch = async (
    steamAccountId: number,
    matchId: string | number
): Promise<StratzMatch> => {
    const query = `{
        match(id: ${matchId}) ${getMatchGraphqlQuery()}
    }`

    const data = JSON.stringify({
        query,
    })

    const response = await postToStratzGraphql<{
        data: { match: StratzMatch }
    }>(data)
    return response.data.match
}

const fetchData = async (
    steamAccountId: number,
    {
        limit = 100,
        beforeMatchId,
        afterMatchId,
        matchIds,
    }: {
        limit?: number
        beforeMatchId?: number
        afterMatchId?: number
        matchIds?: number[]
    }
): Promise<StratzFetchResult> => {
    const query = `{
        player(steamAccountId: ${steamAccountId}) {
            names {
                name
            }
            matchCount
            matches(request: {playerList: ALL, take: ${limit}, ${
                beforeMatchId ? `before: ${beforeMatchId}` : ''
            }${afterMatchId ? `after: ${afterMatchId}` : ''}${
                matchIds ? `matchIds: [${matchIds.join(',')}]` : ''
            }}) ${getMatchGraphqlQuery()}
        }
    }`

    const data = JSON.stringify({
        query,
    })

    const response = await postToStratzGraphql<StratzFetchResult>(data)
    if (response.errors != null && Object.keys(response.errors).length > 0) {
        console.log(JSON.stringify(response.errors, null, 2))
        process.exit()
    }
    return response
}

export const refetchNotParsedMatches = async (
    n: number,
    steamAccountId: number
) => {
    const cache = getDataFromFile(steamAccountId)
    const idsToRefetch = [...cache.matches]
        .reverse()
        .reduce((acc, x) => {
            if (!isParsed(x)) {
                return [...acc, x.id]
            } else {
                return acc
            }
        }, [])
        .slice(0, n)

    const { data } = await fetchData(steamAccountId, {
        limit: n,
        matchIds: idsToRefetch,
    })
    const updatedMatchesMap: { [matchId: string]: StratzMatch } =
        data.player.matches.reduce((acc, x) => {
            if (x.parsedDateTime == null) return acc

            return {
                ...acc,
                [x.id]: x,
            }
        }, {})

    const fetchedMatchesLength = data.player.matches.length
    const updatedMatchesLength = Object.keys(updatedMatchesMap).length
    if (updatedMatchesLength === 0) {
        console.log(
            `Fetched ${G}${fetchedMatchesLength}${NC} matches, but no new parsed match could be added to the cache`
        )
        return
    }

    const newMatches = cache.matches.map((x) => {
        if (updatedMatchesMap[x.id]) {
            return updatedMatchesMap[x.id]
        } else {
            return x
        }
    })
    console.log(
        `Fetched ${G}${fetchedMatchesLength}${NC}. Updated ${G}${updatedMatchesLength}${NC} matches in cache.`
    )
    saveCache({ matches: newMatches }, steamAccountId)
}

export const fetchNew = async (steamAccountId: number, n = 100) => {
    const { matches } = getDataFromFile(steamAccountId)
    const lastMatch = matches.at(-1)

    const { data } = await fetchData(steamAccountId, {
        limit: n,
        afterMatchId: lastMatch.id,
    })
    const newMatches = [...data.player.matches].sort(
        (a, b) => convertDate(a.startDateTime) - convertDate(b.startDateTime)
    )
    let added: StratzMatch[] = []
    let notParsed = 0
    if (newMatches.length) {
        console.log('Old matches length:           ', matches.length)
        newMatches.forEach((nm) => {
            if (nm.parsedDateTime == null) {
                notParsed += 1
            }

            const alreadyIncluded = matches.some((m) => m.id === nm.id)
            if (alreadyIncluded) return

            added.push(nm)
            matches.push(nm)
        })
        if (notParsed > 0) {
            console.log('Matches waiting for parsing:  ', notParsed)
        }
        if (added.length > 0) {
            console.log('Matches added:                ', added.length)
            console.log('New matches length:           ', matches.length)
            console.log(added.map((x) => x.id).join(', '))
        }

        saveCache({ matches }, steamAccountId)
    } else {
        console.log(`No new parsed matches could be fetched`)
    }
}

export const fetchAndSaveAll = async (steamAccountId: number, limit = 500) => {
    let data: { player: StratzPlayerResponse }
    const matches: StratzMatch[] = []
    let nonParsedMatchCount = 0

    do {
        const lastId = data
            ? data.player.matches[data.player.matches.length - 1].id
            : undefined

        data = (
            await fetchData(steamAccountId, {
                limit: limit < 100 ? limit : 100,
                beforeMatchId: lastId,
            })
        ).data
        if (data.player.matches.length > 0) {
            console.log('Pushing: ', data.player.matches.length)
            data.player.matches.forEach((x) => {
                if (x.parsedDateTime == null) {
                    nonParsedMatchCount += 1
                }
            })
            matches.push(...data.player.matches)
        }
    } while (
        data.player.matches.length > 0 &&
        (limit == null || matches.length < limit)
    )

    console.log('Fetched match count:', matches.length)
    if (nonParsedMatchCount) {
        console.log(
            `${R}Data contains ${B}${nonParsedMatchCount}${R} non parsed matches!${NC}`
        )
    }
    const newData = { matches: [...matches].reverse() }
    saveCache(newData, steamAccountId)
    console.log('Matches saved to cache!')

    return newData
}

export const getStatsFromRows = (
    rows: StratzMatch[],
    nums: number[] = [25, 50, 100]
): GameplayStats => {
    const statsGetters: {
        [K in keyof typeof STATS_KEYS]: (x: number) => void
    } = {
        [STATS_KEYS.winrate]: (x) =>
            winrate(rows, x, (r) => getCurrentPlayer(r).isVictory),
        [STATS_KEYS.lanewinrate]: (x) =>
            winrate(
                rows,
                x,
                (r) => getLaneOutcomes(getCurrentPlayer(r), r).isLaneWin
            ),
        [STATS_KEYS.imp]: (x) => averageimp(rows, x),
    }

    return nums.reduce((acc, x) => {
        return {
            ...acc,
            ...Object.entries(statsGetters).reduce((acc, [key, getter]) => {
                return { ...acc, [`${key}${x}`]: getter(x) }
            }, {}),
        }
    }, {})
}

export const getParsedRows = async (steamAccountId: number) => {
    let res = getData(steamAccountId)
    if (res === null) {
        console.log(`Fetching last 500 matches for user ${steamAccountId}...`)
        res = await fetchAndSaveAll(steamAccountId, 500)
    }
    if (res === null) {
        throw new Error(
            'Something went wrong during trying to get data from cache/stratz'
        )
    }
    return { rows: res.matches }
}

export const checkIfReplayAvailable = async (
    matchId: number,
    steamAccountId?: number
) => {
    let replayIdData: ReplayIdData
    let statusCode: number

    if (steamAccountId) {
        const cache = getDataFromFile(steamAccountId)
        const match = cache.matches.find((x) => x.id === matchId)
        if (match) {
            replayIdData = match.replayIdData
        }
    }

    if (replayIdData == null) {
        const res = await get<ReplayIdData[]>(
            'api.opendota.com',
            `/api/replays?match_id=${matchId}`
        )
        if (!res || res.length === 0) {
            console.log(`${R}Response from opendota is empty!${NC}`)
            statusCode = 404
        }

        replayIdData = res[0]

        if (replayIdData && steamAccountId) {
            const cache = getDataFromFile(steamAccountId)
            const newMatches = cache.matches.map((x) => {
                if (x.id === matchId) {
                    return { ...x, replayIdData }
                } else {
                    return x
                }
            })

            saveCache({ ...cache, matches: newMatches }, steamAccountId)
        }
    }

    if (replayIdData) {
        const { cluster, match_id, replay_salt } = replayIdData

        const replayUrl = `http://replay${cluster}.valve.net/570/${match_id}_${replay_salt}.dem.bz2`
        console.log(`Replay url: "${replayUrl}"`)
        const { statusCode: sc } = await head(
            `replay${cluster}.valve.net`,
            `/570/${matchId}_${replay_salt}.dem.bz2`
        )

        statusCode = sc
    }

    if (statusCode === 200) {
        console.log(`${G}Replay for ${matchId} is available!${NC}`)
        return true
    } else if (statusCode === 404) {
        console.log(`${R}Replay for ${matchId} is not available!${NC}`)
    } else {
        console.log(
            `${R}Unexpected status code for ${matchId}: ${statusCode}${NC}`
        )
    }
    return false
}

export const getNotParsedMatches = (steamAccountId: number, n: number) => {
    const cache = getDataFromFile(steamAccountId)
    const notParsedMatches: StratzMatch[] = []

    cache.matches.slice(0, n).forEach((x) => {
        if (!isParsed(x)) {
            notParsedMatches.push(x)
        }
    })

    return notParsedMatches
}

export const requestParseRecentMatches = async (
    steamAccountId: number,
    n: number
) => {
    const notParsedMatches = getNotParsedMatches(steamAccountId, n)

    if (notParsedMatches.length === 0) {
        console.log(`${G}All the ${B}${n}${G} recent matches are parsed!`)
        return
    }

    console.log(
        `${G}Found ${B}${notParsedMatches.length}${G} not parsed match in the last ${B}${n}${NC} matches${NC}`
    )

    let i = 0
    for (const match of notParsedMatches) {
        console.log()
        console.log(
            `${G}Processing ${B}${i + 1}${G} of ${B}${
                notParsedMatches.length
            }${NC}`
        )
        await requestParse(match.id)
        i += 1
    }
    console.log()
    console.log(`${G}Done${NC}`)
}

export const addMatchesFromFile = async (
    steamAccountId: number,
    fileName: string
) => {
    if (!fs.existsSync(fileName)) {
        console.log(`File "${fileName}" doesn't exist`)
        return
    }
    const matchIds = fs.readFileSync(fileName, 'utf-8').trim().split('\n')
    console.log(`Match IDs: ${G}${matchIds.length}${NC}`)
    const fetchedMatches: StratzMatch[] = []

    const { matches } = getDataFromFile(steamAccountId)

    const uniqueMatchIds = [
        ...new Set(
            matchIds.filter((matchId) => {
                return !matches.some((existingMatch) => {
                    return existingMatch.id === Number(matchId)
                })
            })
        ),
    ]

    if (uniqueMatchIds.length !== matchIds.length) {
        console.log(
            `${G}${
                matchIds.length - uniqueMatchIds.length
            }${NC} matches are not unique, skipping them`
        )
    }

    if (uniqueMatchIds.length === 0) {
        console.log('No unique matches were found')
        return
    }

    console.log(`Fetching ${G}${uniqueMatchIds.length}${NC} matches ...`)
    for (const matchId of uniqueMatchIds) {
        try {
            const matchData = await fetchMatch(steamAccountId, matchId)
            if (matchData?.players.length > 0) {
                fetchedMatches.push(matchData)
            } else {
                console.log(
                    `Match "${G}${matchId}${NC}" doesn't have player with "${G}${steamAccountId}${NC}" steam account id, or doesn't exist on stratz, skipping`
                )
            }
        } catch (e) {
            console.log(`Error fetching "${matchId}":`)
            console.log(e)
        }
    }

    const newMatchesToAdd = fetchedMatches
        .filter((match) => {
            return isParsed(match)
        })
        .sort((a, b) => {
            return convertDate(a.startDateTime) - convertDate(b.startDateTime)
        })
    if (newMatchesToAdd.length !== fetchedMatches.length) {
        console.log(
            `${G}${
                fetchedMatches.length - newMatchesToAdd.length
            }${NC} matches were unparsed, skipping those`
        )
    }

    let fetchedMatchesIndex = 0
    let matchesIndex = 0
    const newMatches: StratzMatch[] = []
    while (
        fetchedMatchesIndex < newMatchesToAdd.length ||
        matchesIndex < matches.length
    ) {
        const lastAddedMatchDate =
            convertDate(newMatches.at(-1)?.startDateTime) || 0
        const currentOldMatchToAdd = matches[matchesIndex]

        while (
            newMatchesToAdd[fetchedMatchesIndex] &&
            lastAddedMatchDate <
                convertDate(
                    newMatchesToAdd[fetchedMatchesIndex].startDateTime
                ) &&
            convertDate(newMatchesToAdd[fetchedMatchesIndex].startDateTime) <
                convertDate(
                    currentOldMatchToAdd?.startDateTime ||
                        Number.POSITIVE_INFINITY
                )
        ) {
            newMatches.push(newMatchesToAdd[fetchedMatchesIndex])
            fetchedMatchesIndex += 1
        }

        if (currentOldMatchToAdd) {
            newMatches.push(currentOldMatchToAdd)
        }
        matchesIndex += 1
    }

    saveCache({ matches: newMatches }, steamAccountId)
    console.log(
        `Added ${G}${newMatches.length - matches.length}${NC} new matches`
    )
}

export const refetchMatch = async (matchId: number, steamAccountId: number) => {
    const { matches } = getDataFromFile(steamAccountId)
    const hasMatch = matches.find((x) => x.id === matchId)
    if (!hasMatch) {
        console.log(
            `Match with mathc ID '${
                G + matchId + NC
            }' doesn't exist in the cache.`
        )
        return
    }
    const matchData = await fetchMatch(steamAccountId, matchId)
    if (matchData?.players.length === 0 || !isParsed(matchData)) {
        console.log(
            `Match "${
                G + matchId + NC
            }" isn't parsed, or doesn't exist on stratz, skipping`
        )
        return
    }

    const newMatches = matches.map((x) => {
        if (x.id === matchId) return matchData
        return x
    })

    saveCache({ matches: newMatches }, steamAccountId)
    console.log(
        `Refetched match with match ID '${G + matchId + NC}', and updated cache`
    )
}
