export const setTimeToZero = (a: Date) => {
    const d = new Date(a)
    d.setMilliseconds(0)
    d.setSeconds(0)
    d.setMinutes(0)
    d.setHours(0)
    return d
}

export const getDurationPrettyString = (duration: number) => {
    const durationMinutes = Math.floor(duration / 60)
    const durationSeconds = duration - durationMinutes * 60
    return `${durationMinutes < 10 ? ' ' + durationMinutes : durationMinutes}:${
        durationSeconds < 10 ? '0' + durationSeconds : durationSeconds
    }`
}
