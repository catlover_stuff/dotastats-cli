import https from 'node:https'
import http from 'node:http'
import { NC, R } from './display/colors'

export const post = async <T extends any>(
    hostname: string,
    path: string,
    data: string,
    headers?: { [key: string]: string }
): Promise<T> => {
    return new Promise((resolve, reject) => {
        const options = {
            hostname,
            path,
            port: 443,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length,
                'User-Agent': 'STRATZ_API',
                ...headers,
            },
        }

        const req = https.request(options, (res) => {
            let data = ''

            res.on('data', (d) => {
                data += d
            })
            res.on('end', () => {
                if (res.statusCode !== 200) {
                    console.log(`${R}Status code: ${res.statusCode}${NC}`)
                    console.log(data)
                    process.exit()
                }
                resolve(JSON.parse(data))
            })
        })

        req.on('error', (error) => {
            reject(error)
        })

        console.log('Sending POST request', hostname, path, '...')
        req.write(data)
        req.end()
    })
}

export const head = async (
    hostname: string,
    path: string
): Promise<{ statusCode: number }> => {
    return new Promise((resolve, reject) => {
        let statusCode = -1

        const options = {
            hostname,
            path,
            port: 80,
            method: 'HEAD',
            headers: {
                Accept: '*/*',
                'User-Agent': 'STRATZ_API',
            },
        }

        const req = http.request(options, (res) => {
            statusCode = res.statusCode

            resolve({ statusCode })
            req.abort()
            // res.on('end', () => {})
        })

        req.on('error', (error) => {
            reject(error)
        })

        console.log('Sending HEAD request', hostname, path, '...')
        req.end()
    })
}

export const get = async <T extends any>(
    hostname: string,
    path: string
): Promise<T> => {
    return new Promise((resolve, reject) => {
        const options = {
            hostname,
            path,
            port: 443,
            method: 'GET',
            headers: {
                Accept: '*/*',
                'User-Agent': 'STRATZ_API',
            },
        }

        const req = https.request(options, (res) => {
            let data = ''
            if (res.statusCode !== 200) {
                console.log(`${R}Status code: ${res.statusCode}${NC}`)
                process.exit()
            }

            res.on('data', (d) => {
                data += d
            })
            res.on('end', () => {
                const responseResult = JSON.parse(data)
                resolve(responseResult)
            })
        })

        req.on('error', (error) => {
            reject(error)
        })

        console.log('Sending GET request', hostname, path, '...')
        req.end()
    })
}
