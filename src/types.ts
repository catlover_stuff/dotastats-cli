export const LOBBYTYPE = {
    UNRANKED: 'UNRANKED',
    PRACTICE: 'PRACTICE',
    TOURNAMENT: 'TOURNAMENT',
    TUTORIAL: 'TUTORIAL',
    COOP_VS_BOTS: 'COOP_VS_BOTS',
    TEAM_MATCH: 'TEAM_MATCH',
    SOLO_QUEUE: 'SOLO_QUEUE',
    RANKED: 'RANKED',
    SOLO_MID: 'SOLO_MID',
    BATTLE_CUP: 'BATTLE_CUP',
    EVENT: 'EVENT',
    DIRE_TIDE: 'DIRE_TIDE',
    ALL: 'ALL',
} as const
export type LobbyType = keyof typeof LOBBYTYPE

export const GAMEMODE = {
    NONE: 'NONE',
    ALL_PICK: 'ALL_PICK',
    CAPTAINS_MODE: 'CAPTAINS_MODE',
    RANDOM_DRAFT: 'RANDOM_DRAFT',
    SINGLE_DRAFT: 'SINGLE_DRAFT',
    ALL_RANDOM: 'ALL_RANDOM',
    INTRO: 'INTRO',
    THE_DIRETIDE: 'THE_DIRETIDE',
    REVERSE_CAPTAINS_MODE: 'REVERSE_CAPTAINS_MODE',
    THE_GREEVILING: 'THE_GREEVILING',
    TUTORIAL: 'TUTORIAL',
    MID_ONLY: 'MID_ONLY',
    LEAST_PLAYED: 'LEAST_PLAYED',
    NEW_PLAYER_POOL: 'NEW_PLAYER_POOL',
    COMPENDIUM_MATCHMAKING: 'COMPENDIUM_MATCHMAKING',
    CUSTOM: 'CUSTOM',
    CAPTAINS_DRAFT: 'CAPTAINS_DRAFT',
    BALANCED_DRAFT: 'BALANCED_DRAFT',
    ABILITY_DRAFT: 'ABILITY_DRAFT',
    EVENT: 'EVENT',
    ALL_RANDOM_DEATH_MATCH: 'ALL_RANDOM_DEATH_MATCH',
    SOLO_MID: 'SOLO_MID',
    ALL_PICK_RANKED: 'ALL_PICK_RANKED',
    TURBO: 'TURBO',
    MUTATION: 'MUTATION',
    UNKNOWN: 'UNKNOWN',
    ALL: 'ALL',
} as const
export type GameMode = keyof typeof GAMEMODE

export const PARTY_STATUS = {
    YES: 'YES',
    NO: 'NO',
    BOOTH: 'BOOTH',
} as const
export type PartyStatus = keyof typeof PARTY_STATUS

export type LaneOutcome =
    | 'TIE'
    | 'RADIANT_VICTORY'
    | 'RADIANT_STOMP'
    | 'DIRE_VICTORY'
    | 'DIRE_STOMP'

export type ConvertedLaneOutcome =
    | 'WIN'
    | 'WIN_STOMP'
    | 'TIE'
    | 'LOSS'
    | 'LOSS_STOMP'
    | 'UNKNOWN'

export type AnalysisOutcome = 'NONE' | 'STOMPED' | 'COMEBACK' | 'CLOSE_GAME'

export type Lane =
    | 'ROAMING' // TODO: not handled
    | 'SAFE_LANE'
    | 'MID_LANE'
    | 'OFF_LANE'
    | 'JUNGLE' // TODO: not handled
    | 'UNKNOWN'

export type Role = 'CORE' | 'LIGHT_SUPPORT' | 'HARD_SUPPORT' | 'UNKNOWN'

export const POSITIONS = {
    POSITION_1: 'POSITION_1',
    POSITION_2: 'POSITION_2',
    POSITION_3: 'POSITION_3',
    POSITION_4: 'POSITION_4',
    POSITION_5: 'POSITION_5',
    UNKNOWN: 'UNKNOWN',
    FILTERED: 'FILTERED',
    ALL: 'ALL',
} as const
export type Position = keyof typeof POSITIONS

export type Award = 'NONE' | 'MVP' | 'TOP_CORE' | 'TOP_SUPPORT'

export type ReplayIdData = {
    cluster: number
    match_id: number
    replay_salt: number
}

export type StratzMatch = {
    parsedDateTime: number
    startDateTime: number
    id: number
    winRates: number[]
    radiantNetworthLeads: number[]
    radiantExperienceLeads: number[]
    didRadiantWin: boolean
    direKills: number[]
    radiantKills: number[]
    regionId: number
    predictedWinRates: number[]
    topLaneOutcome: LaneOutcome
    midLaneOutcome: LaneOutcome
    bottomLaneOutcome: LaneOutcome
    rank: number
    analysisOutcome: AnalysisOutcome
    players: StratzGamePlayer[]
    lobbyType: LobbyType
    gameMode: GameMode
    durationSeconds: number

    replayIdData?: ReplayIdData
}

export type StratzGamePlayer = {
    kills: number
    deaths: number
    assists: number
    networth: number
    level: number
    numLastHits: number
    numDenies: number
    steamAccountId: number
    steamAccount: {
        name: string
        seasonRank: number
    }
    stats: {
        itemPurchases: {
            time: number
            itemId: number
        }[]
        level: number
        lastHitsPerMinute: number[]
        deniesPerMinute: number[]
        networthPerMinute: number[]
        experiencePerMinute: number[]
        goldPerMinute: number[]
        heroDamagePerMinute: number[]
        heroDamageReceivedPerMinute: number[]
        healPerMinute: number[]
        campStack: number[]
        allTalks: {
            message: string
            time: number
        }[]
    }
    isVictory: boolean
    isRadiant: boolean
    partyId: number
    lane: Lane
    role: Role
    position: Position
    hero: {
        displayName: string
    }
    imp: number
    award: Award
    backpack0Id: number
    backpack1Id: number
    backpack2Id: number
    item0Id: number
    item1Id: number
    item2Id: number
    item3Id: number
    item4Id: number
    item5Id: number
    neutral0Id: number
    playerSlot: number
}

export type StratzPlayerResponse = {
    matchCount: number
    names: {
        name: string
    }[]
    matches: StratzMatch[]
}

export type Side = 'Dire' | 'Radiant'

export type StratzFetchResult = {
    errors: { message: string }
    data: { player: StratzPlayerResponse }
}

export type GameplayStats = {
    [key: string]: number
}

export type CacheFile = {
    matches: StratzMatch[]
}
