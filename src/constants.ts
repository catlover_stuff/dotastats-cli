import fs from 'node:fs'
import { AnalysisRowTypes } from './display/main-analysis'
import { PlotConfig } from 'asciichart'

import { POSITIONS } from './types'

const configFilePath = './config.json'

try {
    JSON.parse(fs.readFileSync(configFilePath, 'utf8'))
} catch (e) {
    console.log(
        `Error while reading ${configFilePath}, please create a config file!`
    )
    console.log(e.message)
    process.exit(1)
}

export const config: {
    STEAM_ACCOUNT_ID: number
    STRATZ_API_KEY: string
    MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT?: number
    MAIN_ALANYSIS_ROWS?: AnalysisRowTypes[]
    MAIN_ALANYSIS_ROWS_WHEN_VIEWING_OTHER?: AnalysisRowTypes[]
} = JSON.parse(fs.readFileSync(configFilePath, 'utf8'))

export const DATA_WIDTH_LIMIT = process.stdout.columns - 14

export const asciichartConfig: PlotConfig = {
    height: 11,
}

export const DEFAULT_N_ARG = 25
export const DEFAULT_D_ARG = 30

export const BRACKETS = [
    'Unranked',
    'Herald',
    'Guardian',
    'Crusader',
    'Archon',
    'Legend',
    'Ancient',
    'Divine',
    'Immortal',
] as const

export const DETAILED_BRACKETS: { [key: number]: string } = {
    0: 'Unranked',
    ...[
        'Herald',
        'Guardian',
        'Crusader',
        'Archon',
        'Legend',
        'Ancient',
        'Divine',
    ].reduce((acc, bracketName, index) => {
        const subBrackets = {}
        const rankIndex = (index + 1) * 10
        for (let i = 0; i < 5; i++) {
            subBrackets[rankIndex + i] = `${bracketName} ${i + 1}`
        }

        return { ...acc, ...subBrackets }
    }, {}),
    80: 'Immortal',
} as const

export const POSITION_NAMES: { [key in keyof typeof POSITIONS]: string } = {
    [POSITIONS.POSITION_1]: 'Safe lane',
    [POSITIONS.POSITION_2]: 'Mid lane',
    [POSITIONS.POSITION_3]: 'Off lane',
    [POSITIONS.POSITION_4]: 'Support',
    [POSITIONS.POSITION_5]: 'Hard support',
    [POSITIONS.FILTERED]: 'Filtered',
    [POSITIONS.ALL]: 'All',
    [POSITIONS.UNKNOWN]: 'Unknown',
}
