import { getCurrentPlayer, isParsed } from './data-interface'
import { StratzMatch } from './types'

export const winrate = (
    rows: StratzMatch[],
    matchCount: number,
    accessor: (x: StratzMatch) => boolean
) => {
    const dataRows = rows.slice(-matchCount)
    return (
        dataRows.reduce((acc, x) => {
            return acc + (accessor(x) ? 1 : 0)
        }, 0) / dataRows.length
    )
}

export const averageimp = (rows: StratzMatch[], matchCount?: number) => {
    const dataRows = matchCount ? rows.slice(-matchCount) : rows
    const dataLength = dataRows.filter((x) => isParsed(x)).length
    return (
        Math.round(
            (dataRows.reduce((acc, x) => {
                return acc + getCurrentPlayer(x).imp
            }, 0) /
                dataLength) *
                100
        ) / 100
    )
}
