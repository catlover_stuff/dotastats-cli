import {
    ConvertedLaneOutcome,
    LaneOutcome,
    Side,
    StratzGamePlayer,
    StratzMatch,
} from './types'
import { config } from './constants'

export const convertDate = (date: number) => {
    return date * 1000
}

export const getCurrentPlayer = (match: StratzMatch): StratzGamePlayer => {
    return match.players.find(
        (x) => Number(x.steamAccountId) === Number(config.STEAM_ACCOUNT_ID)
    )
}

export const isParsed = (match: StratzMatch) => {
    return match.parsedDateTime != null
}

export const getSide = (player: StratzGamePlayer): Side => {
    return player.isRadiant ? 'Radiant' : 'Dire'
}

const convertLaneOutcome = (
    p: StratzGamePlayer,
    m: StratzMatch,
    lo: LaneOutcome
): ConvertedLaneOutcome => {
    let laneOutcome: ConvertedLaneOutcome

    if (lo === 'TIE') {
        laneOutcome = 'TIE'
    } else if (lo === 'RADIANT_VICTORY') {
        laneOutcome = p.isRadiant ? 'WIN' : 'LOSS'
    } else if (lo === 'RADIANT_STOMP') {
        laneOutcome = p.isRadiant ? 'WIN_STOMP' : 'LOSS_STOMP'
    } else if (lo === 'DIRE_VICTORY') {
        laneOutcome = p.isRadiant ? 'LOSS' : 'WIN'
    } else if (lo === 'DIRE_STOMP') {
        laneOutcome = p.isRadiant ? 'LOSS_STOMP' : 'WIN_STOMP'
    }

    return laneOutcome
}

export const getLaneOutcomes = (
    p: StratzGamePlayer,
    m: StratzMatch
): {
    topLaneOutcome: ConvertedLaneOutcome
    midLaneOutcome: ConvertedLaneOutcome
    bottomLaneOutcome: ConvertedLaneOutcome
    myLaneOutcome: ConvertedLaneOutcome
    otherSideLaneOutcome: ConvertedLaneOutcome

    isLaneWin: boolean
    isLaneLoss: boolean
} => {
    if (m.parsedDateTime == null) {
        return {
            topLaneOutcome: 'UNKNOWN',
            midLaneOutcome: 'UNKNOWN',
            bottomLaneOutcome: 'UNKNOWN',
            myLaneOutcome: 'UNKNOWN',
            otherSideLaneOutcome: 'UNKNOWN',
            isLaneWin: null,
            isLaneLoss: null,
        }
    }
    const topLaneOutcome = convertLaneOutcome(p, m, m.topLaneOutcome)
    const midLaneOutcome = convertLaneOutcome(p, m, m.midLaneOutcome)
    const bottomLaneOutcome = convertLaneOutcome(p, m, m.bottomLaneOutcome)

    let myLaneOutcome: ConvertedLaneOutcome = 'UNKNOWN'
    let otherSideLaneOutcome: ConvertedLaneOutcome = 'UNKNOWN'
    if (p.lane === 'MID_LANE') {
        myLaneOutcome = midLaneOutcome
    } else if (p.isRadiant) {
        if (p.lane === 'SAFE_LANE') {
            myLaneOutcome = bottomLaneOutcome
            otherSideLaneOutcome = topLaneOutcome
        } else {
            myLaneOutcome = topLaneOutcome
            otherSideLaneOutcome = bottomLaneOutcome
        }
    } else {
        if (p.lane === 'SAFE_LANE') {
            myLaneOutcome = topLaneOutcome
            otherSideLaneOutcome = bottomLaneOutcome
        } else {
            myLaneOutcome = bottomLaneOutcome
            otherSideLaneOutcome = topLaneOutcome
        }
    }

    const isLaneWin = myLaneOutcome === 'WIN' || myLaneOutcome === 'WIN_STOMP'
    const isLaneLoss =
        myLaneOutcome === 'LOSS' || myLaneOutcome === 'LOSS_STOMP'

    return {
        topLaneOutcome,
        midLaneOutcome,
        bottomLaneOutcome,
        myLaneOutcome,
        otherSideLaneOutcome,
        isLaneWin,
        isLaneLoss,
    }
}
