import yargs, { Options as YargsOptions } from 'yargs'
import { hideBin } from 'yargs/helpers'

import {
    ConvertedLaneOutcome,
    GAMEMODE,
    GameMode,
    GameplayStats,
    LOBBYTYPE,
    LaneOutcome,
    LobbyType,
    PARTY_STATUS,
    POSITIONS,
    PartyStatus,
    Position,
    StratzFetchResult,
    StratzGamePlayer,
    StratzMatch,
} from './types'
import {
    ANALYSIS_ROW_TYPES,
    AnalysisRowTypes,
    printStats,
} from './display/main-analysis'
import {
    addMatchesFromFile,
    checkIfReplayAvailable,
    fetchAndSaveAll,
    fetchNew,
    getNotParsedMatches,
    getParsedRows,
    refetchMatch,
    refetchNotParsedMatches,
    requestParse,
    requestParseRecentMatches,
} from './stratz'
import {
    printAverageIMPGraph,
    printWinrateGraph,
} from './display/winrate-graphs'
import { printHeroAnalysis } from './display/hero-analysis'
import { DEFAULT_N_ARG, config } from './constants'
import { B, G, NC, R } from './display/colors'
import { FilterConfig, applyD, applyNAndD, filterData } from './data-utils'
import { printHeroWinrateAnalysis } from './display/hero-winrate-analysis'
import { printDailyWinrate } from './display/daily-winrate'
import { printWinrate } from './display/winrate'
import { printGamesByRank } from './display/games-by-rank'
import { expandWithSpaces } from './display/utils/print-utils'
import { printActivity } from './display/activity'
import { listMatches } from './display/match-list'
import { printGamesTillBehaviorScore } from './display/games-till-behavior-score-report'
import { printRankGraph } from './display/rank-graph'
import { printTotalGameTime } from './display/total-game-time'
import { convertDate, getCurrentPlayer } from './data-interface'
import { printLogs } from './display/logs'
import { printMatchDetails } from './display/match'

type COMMAND_TYPE = {
    [key: string]: {
        helpText: string
        requiresArg?: boolean
        positionals?: {
            [name: string]: {
                options: YargsOptions
                isRequired?: boolean
            }
        }
    }
}

// FIXME: preserve keys
const COMMANDS: COMMAND_TYPE = {
    ['*']: {
        helpText: 'List of past matches with the custom configured columns',
    },
    ['match-list']: {
        helpText:
            'Simpler list of matches, similar to the one in the DOTA 2 client',
    },
    ['heros']: {
        helpText: 'Statistics grouped by heroes',
    },
    ['imp']: {
        helpText: 'IMP average graph',
    },
    ['winrate']: {
        helpText:
            'Winrates, lane winrates and IMP-s in the past number of matches',
    },
    ['daily-winrate']: {
        helpText: 'Winrates grouped by the days of the week',
    },
    ['winrate-graph']: {
        helpText: 'Average winrate graph',
    },
    ['hero-winrates']: {
        helpText:
            'Average hero winrates in columns, where each column considers a different amount of past matches',
    },
    ['games-by-rank']: {
        helpText: 'Average winrate, lane winrate and IMP grouped by rank',
    },
    ['activity']: {
        helpText: 'Activity calendar',
    },
    ['rank-graph']: {
        helpText: 'Graph based on games ranks',
    },
    ['game-time']: {
        helpText: 'Total game time of the matches',
    },
    ['games-till-behavior-score-report']: {
        helpText: 'How many games are left till the next behavior score report',
    },
    ['match']: {
        helpText: 'Details about the specified match ID',
        positionals: {
            matchid: {
                options: {
                    description: 'Match ID',
                    type: 'number',
                },
                isRequired: true,
            },
        },
    },
    ['logs']: {
        helpText: 'Chat events of the specified mathc ID',
        positionals: {
            matchid: {
                options: {
                    description: 'Match ID',
                    type: 'number',
                },
                isRequired: true,
            },
        },
    },

    ['refetch']: {
        helpText: 'Refetched the match with the specified match ID',
        positionals: {
            matchid: {
                options: {
                    description: 'Match ID',
                    type: 'number',
                },
                isRequired: true,
            },
        },
    },
    ['refetch-all']: {
        helpText:
            'Refetches cache, overwriting the cache. By defviaault gets last 500 matches, overwrite it with n',
    },
    ['refetch-unparsed']: {
        helpText: 'Refetches last n matches which are not parsed in the cache',
    },
    ['fetch-new']: {
        helpText: 'Fetches matches since the latest match in the cache',
    },
    ['add-matches-from-file']: {
        helpText:
            'Adds matches from file. File should be specified with "--file-name", and should contain one match ID per row\n',
    },

    ['request-parse']: {
        helpText: 'Asks stratz to parse the supplied match id',
        requiresArg: true,
        positionals: {
            matchid: {
                options: {
                    description: 'Match ID',
                    type: 'number',
                },
                isRequired: true,
            },
        },
    },
    ['request-parse-recents']: {
        helpText:
            'Checks if replay is available for the last n unparsed matches in cache, and if yes asks stratz to parse them',
    },
    ['check-replay-available']: {
        helpText: 'Checks if a match replay is available via opendota',
        requiresArg: true,
        positionals: {
            matchId: {
                options: {
                    description: 'Match ID',
                    type: 'number',
                },
                isRequired: true,
            },
        },
    },
    ['print-not-parsed-matches']: {
        helpText: 'Latests unparsed matches in cache',
    },
} as const

type CommandType = {
    [K in keyof typeof COMMANDS]: {
        requiresData?: boolean
        action: (param?: any) => void
    }
}

const CMD_OPTIONS: { [key: string]: YargsOptions } = {
    num: {
        description:
            'Number of matches the command should process, some commands use this as days',
        requiresArg: true,
        number: true,
    },
    day: {
        description:
            'Games from the past X days will be used in the calculations',
        requiresArg: true,
        number: true,
    },
    'lobby-type': {
        description: 'Which lobby types to operate on',
        default: [],
        choices: Object.keys(LOBBYTYPE),
        array: true,
    },
    'party-status': {
        description: 'If player was in party or not',
        default: PARTY_STATUS.BOOTH,
        choices: Object.keys(PARTY_STATUS),
    },
    'game-mode': {
        description: 'Which game mode to operate on',
        default: [],
        choices: Object.keys(GAMEMODE),
        array: true,
    },
    match: {
        description: 'Id of match to use for command',
        default: [],
        array: true,
    },
    hero: {
        description: 'Which heroes to show',
        default: [],
        array: true,
    },
    position: {
        description: 'Which position to show',
        default: [],
        choices: Object.keys(POSITIONS),
        array: true,
    },
    'main-analysis-rows': {
        description: 'Main analysis rows to show',
        default: config.MAIN_ALANYSIS_ROWS || [],
        choices: Object.keys(ANALYSIS_ROW_TYPES),
        array: true,
    },
    'steam-account-id': {
        description:
            'Which steam account id should be used to overwrite the default',
        requiresArg: true,
        default: config.STEAM_ACCOUNT_ID,
    },
    h: { alias: 'help' },
    m: { alias: 'match' },
    n: { alias: 'num' },
    d: { alias: 'day' },
    l: { alias: 'lobby-type' },
    g: { alias: 'game-mode' },
    p: { alias: 'position' },
    r: { alias: 'main-analysis-rows' },
    sid: { alias: 'steam-account-id' },
} as const

const yargsSetup = yargs(hideBin(process.argv))
    .wrap(Math.min(100, process.stdout.columns))
    .scriptName('')
    .usage('Dota cli stats script')

Object.keys(COMMANDS).forEach((command) => {
    const { helpText, requiresArg, positionals } = COMMANDS[command]
    let cmdString = command
    const setupArgs: [any?, any?, any?] = [helpText]

    if (positionals) {
        Object.entries(positionals).forEach(([name, { isRequired }]) => {
            cmdString += isRequired ? ` <${name}>` : ` [${name}]`
        })

        setupArgs.push((y) => {
            Object.entries(positionals).forEach(
                ([name, { options, isRequired }]) => {
                    y.positional(name, options)
                }
            )
        })
    }

    setupArgs.unshift(cmdString)

    yargsSetup.command(...setupArgs)

    if (requiresArg) {
        yargsSetup.requiresArg(cmdString)
    }
})

Object.entries(CMD_OPTIONS).forEach(([key, value]) => {
    yargsSetup.option(key, value)
})

yargsSetup.parse()
const argv = yargsSetup.argv

const d = parseInt(argv['d'])
const n =
    Number.isNaN(+d) && Number.isNaN(+parseInt(argv['n']))
        ? DEFAULT_N_ARG
        : parseInt(argv['n'])
const hasNArg = !(yargsSetup.parsed as any).defaulted['num']
const hasSteamAccountIdArg = !(yargsSetup.parsed as any).defaulted[
    'steam-account-id'
]
const steamAccountId: number = argv['steam-account-id']
const mainAnalysisRows: AnalysisRowTypes[] = argv['main-analysis-rows']

const lobbyType: LobbyType[] = argv['lobby-type']
const position: Position[] = argv['position']
const gameMode: GameMode[] = argv['game-mode']
const hero: string[] = argv['hero']
const partyStatus: PartyStatus = argv['party-status']

const filterConfig: FilterConfig = {
    position,
    lobbyType,
    gameMode,
    partyStatus,
    hero,
    d,
    n,
}

let parsedRows: {
    rows: StratzMatch[]
} = null

const COMMAND_ACTIONS: CommandType = {
    ['*']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            const printConfigToUse = hasSteamAccountIdArg
                ? config.MAIN_ALANYSIS_ROWS_WHEN_VIEWING_OTHER
                : mainAnalysisRows

            printStats(applyNAndD(rows, n, d), printConfigToUse)
        },
    },
    ['list-matches']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            listMatches(applyNAndD(rows, n, d))
        },
    },
    ['heros']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            printHeroAnalysis(applyNAndD(rows, n, d))
        },
    },
    ['imp']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            printAverageIMPGraph(rows, n)
        },
    },
    ['winrate']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            printWinrate(rows)
        },
    },
    ['daily-winrate']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            printDailyWinrate(applyNAndD(rows, n, d))
        },
    },
    ['winrate-graph']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()

            printWinrateGraph(rows, n, { height: 20 })
        },
    },
    ['hero-winrates']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printHeroWinrateAnalysis(rows)
        },
    },
    ['games-by-rank']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printGamesByRank(applyNAndD(rows, n, d))
        },
    },
    ['activity']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printActivity(applyNAndD(rows, n, d))
        },
    },
    ['rank-graph']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printRankGraph(applyNAndD(rows, n, d))
        },
    },
    ['game-time']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printTotalGameTime(applyNAndD(rows, n, d))
        },
    },
    ['games-till-behavior-score-report']: {
        requiresData: true,
        action: async () => {
            const { rows } = await getParsedRowsFromCache()
            printGamesTillBehaviorScore(rows)
        },
    },
    ['match']: {
        requiresData: true,
        action: async (matchId) => {
            const { rows } = await getParsedRowsFromCache()
            const match = rows.find((x) => x.id === matchId)
            if (match == null) {
                console.log(
                    `Couldn't find match with ID '${matchId}', please provide one with '--match' or '-m'`
                )
                return
            }
            printMatchDetails(match)
        },
    },
    ['logs']: {
        requiresData: true,
        action: async (matchId) => {
            const { rows } = await getParsedRowsFromCache()
            const match = rows.find((x) => x.id === matchId)
            if (match == null) {
                console.log(
                    `Couldn't find match with ID '${matchId}', please provide one with '--match' or '-m'`
                )
                return
            }
            printLogs(match)
        },
    },

    ['refetch']: {
        action: async (matchId) => {
            if (matchId == null) {
                console.log(
                    `No match ID provided, please provide one with '--match' or '-m'`
                )
                return
            }
            await refetchMatch(matchId, steamAccountId)
        },
    },
    ['refetch-all']: {
        action: async () => {
            await fetchAndSaveAll(steamAccountId, hasNArg ? n : undefined)
        },
    },
    ['refetch-unparsed']: {
        action: async () => {
            await refetchNotParsedMatches(n, steamAccountId)
        },
    },
    ['fetch-new']: {
        action: async () => {
            await fetchNew(steamAccountId, hasNArg ? n : undefined)
        },
    },
    ['add-matches-from-file']: {
        action: async () => {
            const fileName: string = argv['file-name']
            await addMatchesFromFile(steamAccountId, fileName)
        },
    },

    ['request-parse']: {
        action: async (matchId) => {
            if (!Number.isNaN(matchId) && matchId > 2) {
                await requestParse(matchId)
            } else {
                console.log("Couldn't parse replay ID", matchId)
            }
        },
    },
    ['request-parse-recents']: {
        action: async () => {
            await requestParseRecentMatches(steamAccountId, n)
        },
    },
    ['check-replay-available']: {
        action: async (matchId) => {
            if (!Number.isNaN(matchId) && matchId > 2) {
                await checkIfReplayAvailable(matchId, steamAccountId)
            } else {
                console.log("Couldn't parse replay ID", matchId)
            }
        },
    },
    ['print-not-parsed-matches']: {
        action: async () => {
            const matches = getNotParsedMatches(steamAccountId, n)
            matches.forEach((x) => {
                console.log({
                    matchId: x.id,
                    isVictory: getCurrentPlayer(x).isVictory,
                    hero: getCurrentPlayer(x).hero.displayName,
                    date: new Date(
                        convertDate(x.startDateTime)
                    ).toLocaleString(),
                })
            })
        },
    },
} as const

const getParsedRowsFromCache = async () => {
    if (!parsedRows) {
        const parsedData = await getParsedRows(steamAccountId)
        parsedRows = {
            rows: filterData(parsedData.rows, filterConfig),
        }

        if (parsedRows.rows.length === 0) {
            console.log(`${R}0 matches match these filters!${NC}`)
        }

        console.log(`${B}Filters${NC}:`, JSON.stringify(filterConfig))
        if (Number.isNaN(+n) && Number.isNaN(+d)) {
            console.log(
                `${B}Match count${NC}: ${G}${parsedRows.rows.length}${NC}`
            )
        } else if (!Number.isNaN(+n)) {
            console.log(`${B}Match count${NC}: ${G}${n}${NC}`)
        } else if (!Number.isNaN(+d)) {
            const dateFrom = new Date()
            dateFrom.setDate(dateFrom.getDate() - d)
            dateFrom.setMilliseconds(0)
            dateFrom.setSeconds(0)
            dateFrom.setMinutes(0)
            dateFrom.setHours(0)

            if (Number.isNaN(+n)) {
                // no other way to find out match count
                const rows = applyD(parsedRows.rows, d)
                console.log(`${B}Match count${NC}: ${G}${rows.length}${NC}`)
            }
            console.log(
                `${B}Date range${NC}: ${G}${dateFrom.toLocaleDateString()}${NC} ${B}-${NC} ${G}${new Date().toLocaleDateString()}${NC}`
            )
        }
    }
    return parsedRows
}

const commandsRequiringData = Object.entries(COMMAND_ACTIONS).reduce(
    (acc, [key, command]) => {
        if (command.requiresData) return [...acc, key]
        return acc
    },
    []
)
const commands: (keyof typeof COMMAND_ACTIONS)[] = argv['_']
const shouldLoadData = commands.some((c) => {
    return commandsRequiringData.includes(c)
})

//
;(async () => {
    if (shouldLoadData) {
        await getParsedRowsFromCache()
    }
    if (hasSteamAccountIdArg) {
        console.log(`Showing data for steam user ${G}${steamAccountId}${NC}`)
    }

    if (commands.length === 0) {
        await COMMAND_ACTIONS['*'].action()
    }
    for (const command of commands) {
        const commandToRun = COMMAND_ACTIONS[command]
        if (!commandToRun) {
            console.log('Invalid command:', command)
            continue
        }

        if (commands.length > 1) {
            console.log(
                expandWithSpaces(
                    ' -- ' + command.toString() + ' ',
                    process.stdout.columns - 1,
                    'CENTER',
                    '-'
                )
            )
        }
        await commandToRun.action(argv['matchid'])
    }
})()
