import { ALL_COLOR_CODES, COLORS, NC } from '../colors'

const COLOR_CODE_REGEX = '\\x1b\\[[0-9]{1,2}m'

const getTextWithoutColorCodes = (text: string) => {
    if (!text) return ''
    const re = new RegExp(COLOR_CODE_REGEX, 'g')
    return text.replace(re, '')
}

export const percent = (n: number, whole = true) => {
    if (Number.isNaN(n)) return 'NaN'
    if (whole) {
        return `${Math.round(n * 100)}%`
    } else {
        return `${Math.round(n * 1000) / 10}%`
    }
}

type ALIGN = 'LEFT' | 'CENTER' | 'RIGHT'

export const expandWithSpaces = (
    text: string,
    n: number,
    align: ALIGN = 'LEFT',
    expandCharacter = ' '
) => {
    const textWithoutColorCodes = getTextWithoutColorCodes(text)

    if (textWithoutColorCodes.length > n) {
        if (textWithoutColorCodes.length <= n) {
            return text
        }

        const colorCodeIndexes = []
        let match
        const re = new RegExp(COLOR_CODE_REGEX, 'g')
        while ((match = re.exec(text))) {
            console.log
            colorCodeIndexes.push([match.index, re.lastIndex])
        }

        const charsNeededToBeRemoved = textWithoutColorCodes.length - n - 1
        let charsRemoved = 0
        let res = ''
        let i = text.length - 1
        while (i >= 0) {
            const isCurrentIndexInColorCode = colorCodeIndexes.some(
                ([start, end]) => {
                    return i >= start && i <= end
                }
            )

            if (isCurrentIndexInColorCode) {
                res = text[i] + res
            } else {
                if (charsRemoved > charsNeededToBeRemoved) {
                    res = text[i] + res
                } else {
                    charsRemoved += 1
                }
            }

            i = i - 1
        }

        return res
    } else if (textWithoutColorCodes.length < n) {
        const colorCodeDifference = n - textWithoutColorCodes.length - 1
        if (align === 'CENTER') {
            let res = text
            let addToEnd = true
            for (let i = 0; i <= colorCodeDifference; i++) {
                if (addToEnd) {
                    res = res + expandCharacter
                } else {
                    res = expandCharacter + res
                }
                addToEnd = !addToEnd
            }

            return res
        } else if (align === 'RIGHT') {
            let res = text
            for (let i = 0; i <= colorCodeDifference; i++) {
                res = expandCharacter + res
            }

            return res
        } else {
            let res = text
            for (let i = 0; i <= colorCodeDifference; i++) {
                res += expandCharacter
            }

            return res
        }
    } else {
        return text
    }
}

export const logJustified = (toLog: string[], colWidth?: number) => {
    const cw =
        colWidth ||
        Math.floor(Math.min(process.stdout.columns - 20, 120) / toLog.length)

    const rowString = toLog
        .map((x) => {
            return expandWithSpaces(x, cw)
        })
        .join('  ')
    console.log(rowString)
}

export const joinGraphs = (...graphs: string[]) => {
    const graphRows: string[] = []
    const maxRow = Math.max(...graphs.map((x) => x.split('\n').length))

    for (let i = 0; i < maxRow; i++) {
        graphs.forEach((graph) => {
            graphRows[i] =
                (graphRows[i] != null ? graphRows[i] : '') +
                graph.split('\n')[i]
        })
    }

    return graphRows.join('\n')
}

export const getColoredString = (val: string, colorBy?: string) => {
    const str = colorBy || val
    if (str == null) return ''
    const nameMod = Array.from(str).reduce((acc, x) => acc + x.charCodeAt(0), 0)
    const c = COLORS[nameMod % COLORS.length]
    return `${c}${val}${NC}`
}

export const getTimeStamp = (totalSeconds: number) => {
    const minutes = Math.floor(totalSeconds / 60)
    const seconds = Math.abs(totalSeconds) - minutes * 60
    return `${minutes}:${seconds < 10 ? '0' + seconds : seconds}`
}

export const duplicateEachValueInArray = <T>(array: T[]): T[] => {
    return array.reduce((acc, x) => [...acc, x, x], [])
}
