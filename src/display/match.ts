import {
    DETAILED_BRACKETS,
    POSITION_NAMES,
    asciichartConfig,
    config,
} from '../constants'
import { convertDate } from '../data-interface'
import { LaneOutcome, POSITIONS, StratzGamePlayer, StratzMatch } from '../types'
import { B, BG, Black, G, M, NC, R, Y } from './colors'
import { printLogs } from './logs'
import {
    duplicateEachValueInArray,
    expandWithSpaces,
    getColoredString,
    getTimeStamp,
    percent,
} from './utils/print-utils'
import {
    plot,
    green as chartColorGreen,
    blue as chartColorBlue,
    black as chartColorBlack,
    white as chartColorWhite,
} from 'asciichart'

const PLAYER_COLUMN_WIDTH = 14

const PLAYER_ORDER_BY_POSITION: (keyof typeof POSITIONS)[] = [
    POSITIONS.POSITION_1,
    POSITIONS.POSITION_2,
    POSITIONS.POSITION_3,
    POSITIONS.POSITION_4,
    POSITIONS.POSITION_5,
] as const

const createPlayerOrder = (players: StratzGamePlayer[]) => {
    const radiantTeamWithoutPositions: StratzGamePlayer[] = []
    const radiantTeam: StratzGamePlayer[] = []
    const direTeamWithoutPositions: StratzGamePlayer[] = []
    const direTeam: StratzGamePlayer[] = []

    players.forEach((p) => {
        const positionIndex = PLAYER_ORDER_BY_POSITION.indexOf(p.position)
        if (positionIndex === -1) {
            p.isRadiant
                ? radiantTeamWithoutPositions.push(p)
                : direTeamWithoutPositions.push(p)
        }

        p.isRadiant
            ? (radiantTeam[positionIndex] = p)
            : (direTeam[positionIndex] = p)
    })

    return [
        ...radiantTeam,
        ...radiantTeamWithoutPositions,
        ...direTeam,
        ...direTeamWithoutPositions,
    ]
}

const getLaneOutcomeString = (laneOutcome: LaneOutcome) => {
    if (laneOutcome === 'DIRE_VICTORY' || laneOutcome === 'DIRE_STOMP') {
        return R + (laneOutcome === 'DIRE_VICTORY' ? 'Win' : 'Stomp') + NC
    } else if (
        laneOutcome === 'RADIANT_VICTORY' ||
        laneOutcome === 'RADIANT_STOMP'
    ) {
        return G + (laneOutcome === 'RADIANT_VICTORY' ? 'Win' : 'Stomp') + NC
    } else {
        return 'Tie'
    }
}

const printLaneOutcomes = (match: StratzMatch) => {
    const { players } = match
    const rTop = players.filter((p) => p.lane === 'SAFE_LANE' && p.isRadiant)
    const dTop = players.filter((p) => p.lane === 'OFF_LANE' && !p.isRadiant)
    const rMid = players.filter((p) => p.lane === 'MID_LANE' && p.isRadiant)
    const dMid = players.filter((p) => p.lane === 'MID_LANE' && !p.isRadiant)
    const rBot = players.filter((p) => p.lane === 'OFF_LANE' && p.isRadiant)
    const dBot = players.filter((p) => p.lane === 'SAFE_LANE' && !p.isRadiant)

    const laneHeroesStringWidth = 48
    const heroStringWidth = (laneHeroesStringWidth - 4) / 2

    const printLane = (
        title: string,
        direPlayers: StratzGamePlayer[],
        radiantPlayers: StratzGamePlayer[],
        laneOutcome: LaneOutcome
    ) => {
        const outcomeWidth = 8
        console.log(
            title,
            expandWithSpaces(
                laneOutcome === 'RADIANT_STOMP' ||
                    laneOutcome === 'RADIANT_VICTORY' ||
                    laneOutcome === 'TIE'
                    ? getLaneOutcomeString(laneOutcome)
                    : '',
                outcomeWidth
            ),
            G + '|' + NC,
            expandWithSpaces(
                direPlayers
                    .map((x) =>
                        expandWithSpaces(
                            getColoredString(x.hero.displayName),
                            heroStringWidth
                        )
                    )
                    .join(', '),
                laneHeroesStringWidth
            ),
            ' VS ',
            expandWithSpaces(
                radiantPlayers
                    .map((x) =>
                        expandWithSpaces(
                            getColoredString(x.hero.displayName),
                            heroStringWidth
                        )
                    )
                    .join(', '),
                laneHeroesStringWidth
            ),
            G + '|' + NC,
            expandWithSpaces(
                laneOutcome === 'DIRE_STOMP' ||
                    laneOutcome === 'DIRE_VICTORY' ||
                    laneOutcome === 'TIE'
                    ? getLaneOutcomeString(laneOutcome)
                    : '',
                outcomeWidth
            )
        )
    }

    printLane('Top lane: ', rTop, dTop, match.topLaneOutcome)
    printLane('Mid lane: ', rMid, dMid, match.midLaneOutcome)
    printLane('Bot lane: ', rBot, dBot, match.bottomLaneOutcome)
}

const SEPARATOR = `${G}|${NC}`
const logRow = (
    players: StratzGamePlayer[],
    accessor: (x: StratzGamePlayer) => string
) => {
    console.log(
        players
            .map((x, i) => {
                let v = expandWithSpaces(
                    accessor(x),
                    PLAYER_COLUMN_WIDTH,
                    'CENTER'
                )
                if (i === 0) {
                    //
                } else if (i === players.length - 1) {
                    v = SEPARATOR + v
                } else if (i === 5) {
                    v = '   ' + v
                } else {
                    v = SEPARATOR + v
                }
                return v
            })
            .join('')
    )
}

const HEADER_WIDTH = (PLAYER_COLUMN_WIDTH * 5 + 4) * 2
const printHeader = (match: StratzMatch) => {
    const totalDireKills = match.direKills.reduce((acc, x) => acc + x, 0)
    const totalRadianKills = match.radiantKills.reduce((acc, x) => acc + x, 0)

    console.log(
        expandWithSpaces(
            (match.didRadiantWin ? G + 'Radiant' : R + 'Dire') +
                ' victory' +
                (match.analysisOutcome !== 'NONE'
                    ? ' (' + match.analysisOutcome + ')'
                    : '') +
                NC,
            HEADER_WIDTH,
            'CENTER'
        )
    )
    console.log(
        expandWithSpaces(
            G + totalRadianKills + NC + ' - ' + R + totalDireKills + NC,
            HEADER_WIDTH,
            'CENTER'
        )
    )
    console.log(
        expandWithSpaces(
            `${getTimeStamp(match.durationSeconds)}`,
            HEADER_WIDTH,
            'CENTER'
        )
    )
    console.log(
        expandWithSpaces(
            G +
                new Date(convertDate(match.startDateTime)).toLocaleString() +
                NC +
                ' ' +
                getColoredString(match.gameMode) +
                ' ' +
                getColoredString(match.lobbyType),

            HEADER_WIDTH,
            'CENTER'
        )
    )
    console.log()
}

const printMatchWinrateGraph = (match: StratzMatch) => {
    console.log()

    console.log(B + 'predictedWinRates' + NC, G + 'winRates' + NC)
    console.log(
        plot(
            [
                match.winRates.map(() => 1),
                match.winRates.map(() => 0),
                match.winRates.map(() => 0.5),
                match.predictedWinRates.slice(0, match.winRates.length),
                match.winRates,
            ].map((x) => duplicateEachValueInArray(x)),
            {
                ...asciichartConfig,
                height: 14,
                format: (x) => expandWithSpaces(percent(x), 5),
                colors: [
                    chartColorBlack,
                    chartColorBlack,
                    chartColorWhite,
                    chartColorBlue,
                    chartColorGreen,
                ],
            }
        )
    )
}

const printGoldXpAdvantageGraph = (match: StratzMatch) => {
    const absMax = Math.max(
        ...match.radiantNetworthLeads.map((x) => Math.abs(x))
    )
    console.log(
        B + 'radiantExperienceLeads' + NC,
        G + 'radiantNetworthLeads' + NC
    )
    console.log(
        plot(
            [
                match.radiantNetworthLeads.map(() => absMax),
                match.radiantNetworthLeads.map(() => -absMax),
                match.radiantNetworthLeads.map(() => 0),
                match.radiantExperienceLeads || [],
                match.radiantNetworthLeads,
            ].map((x) => duplicateEachValueInArray(x)),
            {
                ...asciichartConfig,
                height: 14,
                format: (x) =>
                    expandWithSpaces(Math.floor(x).toLocaleString(), 7),
                colors: [
                    chartColorBlack,
                    chartColorBlack,
                    chartColorWhite,
                    chartColorBlue,
                    chartColorGreen,
                ],
            }
        )
    )
}

export const printMatchDetails = (match: StratzMatch) => {
    const orderedPlayers = createPlayerOrder(match.players)

    printHeader(match)

    logRow(orderedPlayers, (x) =>
        getColoredString(x.hero.displayName.toString())
    )
    logRow(orderedPlayers, (x) => (x.imp < 0 ? R : G) + x.imp + NC)
    logRow(orderedPlayers, (x) => {
        if (x.award === 'TOP_CORE') return R + x.award + NC
        if (x.award === 'TOP_SUPPORT') return M + x.award + NC
        if (x.award === 'MVP') return B + x.award + NC
        return ''
    })
    logRow(orderedPlayers, (x) => getColoredString(POSITION_NAMES[x.position]))
    logRow(
        orderedPlayers,
        (x) =>
            `${G}${x.kills}${NC} / ${R}${x.deaths}${NC} / ${B}${x.assists}${NC}`
    )
    logRow(orderedPlayers, (x) => Y + x.networth.toLocaleString() + NC)
    logRow(orderedPlayers, (x) => x.level.toString())
    logRow(orderedPlayers, (x) => {
        if (x.steamAccountId === config.STEAM_ACCOUNT_ID) {
            return Black + BG + x.steamAccount.name + NC
        } else {
            return x.steamAccount.name
        }
    })
    logRow(orderedPlayers, (x) =>
        getColoredString(
            DETAILED_BRACKETS[x.steamAccount.seasonRank - 1] || '?'
        )
    )
    console.log()

    printLaneOutcomes(match)
    printMatchWinrateGraph(match)
    printGoldXpAdvantageGraph(match)
    console.log()

    printLogs(match)
}
