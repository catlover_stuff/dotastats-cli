import { averageimp } from '../calculation-helpers'
import { getCurrentPlayer, getLaneOutcomes, isParsed } from '../data-interface'
import { ConvertedLaneOutcome, StratzMatch } from '../types'
import { B, COLORS, G, NC, R } from './colors'
import { expandWithSpaces, percent } from './utils/print-utils'

type HeroAnalitics = {
    matches: number
    wins: number
    winPercent: number
    losses: number
    averageImp: number
    laneWins: number
    laneLosses: number
    laneWinPercent: number
    laneLossPercent: number
}

const w = (x) => process.stdout.write(x)

const getLanePercent = (rows: StratzMatch[]) => {
    let laneWinCount = 0
    let laneLossCount = 0
    const parsedCount = rows.reduce((acc, x) => acc + (isParsed(x) ? 1 : 0), 0)

    rows.forEach((x) => {
        const { isLaneWin, isLaneLoss } = getLaneOutcomes(
            getCurrentPlayer(x),
            x
        )
        if (isLaneWin) {
            laneWinCount += 1
        } else if (isLaneLoss) {
            laneLossCount += 1
        }
    })
    return {
        laneWinPercent: Math.round((laneWinCount / parsedCount) * 100) / 100,
        laneLossPercent: Math.round((laneLossCount / parsedCount) * 100) / 100,
    }
}

const getHeroAnalysis = (rows: StratzMatch[], n = 100) => {
    const groupedHeroes: {
        [key: string]: StratzMatch[]
    } = {}

    rows.forEach((x) => {
        const {
            hero: { displayName },
        } = getCurrentPlayer(x)
        if (!groupedHeroes[displayName]) {
            groupedHeroes[displayName] = []
        }
        groupedHeroes[displayName] = [...groupedHeroes[displayName], x]
    })

    const heroAnalytics: { [key: string]: HeroAnalitics } = Object.entries(
        groupedHeroes
    ).reduce<{ [key: string]: HeroAnalitics }>((acc, [heroName, heroRows]) => {
        const { laneWinPercent, laneLossPercent } = getLanePercent(heroRows)
        const wins = heroRows.reduce(
            (acc, x) => acc + (getCurrentPlayer(x).isVictory ? 1 : 0),
            0
        )

        acc[heroName] = {
            matches: heroRows.length,
            wins,
            winPercent: Math.round((wins / heroRows.length) * 100) / 100,
            losses: heroRows.reduce(
                (acc, x) => acc + (getCurrentPlayer(x).isVictory ? 0 : 1),
                0
            ),
            averageImp: averageimp(heroRows),
            laneWins: heroRows.reduce((acc, x) => {
                const { isLaneWin } = getLaneOutcomes(getCurrentPlayer(x), x)
                return acc + (isLaneWin ? 0 : 1)
            }, 0),
            laneLosses: heroRows.reduce((acc, x) => {
                const { isLaneLoss } = getLaneOutcomes(getCurrentPlayer(x), x)
                return acc + (isLaneLoss ? 0 : 1)
            }, 0),
            laneWinPercent,
            laneLossPercent,
        }

        return acc
    }, {})

    return heroAnalytics
}

const NAME_COL_WIDTH = 16
const NUM_COL_WIDTH = 10

const printHeader = (text: string, color: string, width: number) => {
    w(expandWithSpaces(`${color}${text}${NC}`, width))
}

const printCell = (text: string | number, color: string, width: number) => {
    w(expandWithSpaces(`${color}${text}${NC}`, width))
}

export const printHeroAnalysis = (rows: StratzMatch[]) => {
    const heroAnalytics = getHeroAnalysis(rows)

    printHeader('Hero name', B, NAME_COL_WIDTH)
    printHeader('Matches', G, NUM_COL_WIDTH)
    printHeader('Wins', G, NUM_COL_WIDTH)
    printHeader('losses', G, NUM_COL_WIDTH)
    printHeader('Win %', G, NUM_COL_WIDTH)
    printHeader('Avg IMP', G, NUM_COL_WIDTH)
    printHeader('Lane win', G, NUM_COL_WIDTH)
    printHeader('Lane loss', G, NUM_COL_WIDTH)
    w('\n')

    Object.entries(heroAnalytics)
        .sort((a, b) => {
            return b[1].matches - a[1].matches
        })
        .forEach(([heroName, analytics], i) => {
            if (i % 5 === 0) {
                console.log()
            }
            const nameMod = Array.from(heroName).reduce(
                (acc, x) => acc + x.charCodeAt(0),
                0
            )
            const c = COLORS[nameMod % COLORS.length]

            printCell(heroName, c, NAME_COL_WIDTH)
            printCell(analytics.matches, B, NUM_COL_WIDTH)
            printCell(analytics.wins, G, NUM_COL_WIDTH)
            printCell(analytics.losses, R, NUM_COL_WIDTH)
            printCell(
                percent(analytics.winPercent),
                analytics.winPercent < 0.5 ? R : G,
                NUM_COL_WIDTH
            )
            printCell(
                analytics.averageImp,
                analytics.averageImp < 0.5 ? R : G,
                NUM_COL_WIDTH
            )
            printCell(
                percent(analytics.laneWinPercent),
                analytics.laneWinPercent < 0.5 ? R : G,
                NUM_COL_WIDTH
            )
            printCell(
                percent(analytics.laneLossPercent),
                analytics.laneLossPercent > 0.5 ? R : G,
                NUM_COL_WIDTH
            )

            w('\n')
        })
}
