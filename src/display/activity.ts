import { winrate } from '../calculation-helpers'
import { convertDate, getCurrentPlayer } from '../data-interface'
import { StratzMatch } from '../types'
import { setTimeToZero } from '../utils'
import { B, G, NC, R } from './colors'
import { expandWithSpaces, percent } from './utils/print-utils'

const DAY_ORDER = [1, 2, 3, 4, 5, 6, 0]
const DAY_SEPARATOR = `${B}|${NC}`
const EMPTY_CELL = '       '

const getHeaderString = () => {
    const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    // Sunday - Saturday : 0 - 6
    const dayIndex = new Date().getDay()
    const nowIndex = DAY_ORDER.findIndex((x) => x === dayIndex)
    days[nowIndex] = G + days[nowIndex] + NC
    return (
        `${DAY_SEPARATOR}   ${days[0]}` +
        `  ${DAY_SEPARATOR}   ${days[1]}` +
        `  ${DAY_SEPARATOR}   ${days[2]}` +
        `  ${DAY_SEPARATOR}   ${days[3]}` +
        `  ${DAY_SEPARATOR}   ${days[4]}` +
        `  ${DAY_SEPARATOR}   ${days[5]}` +
        `  ${DAY_SEPARATOR}   ${days[6]}` +
        `   ${DAY_SEPARATOR}${DAY_SEPARATOR}   Sum`
    )
}

const dateDiffInDays = (a: Date, b: Date) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24
    const dates = [
        Date.UTC(a.getFullYear(), a.getMonth(), a.getDate()),
        Date.UTC(b.getFullYear(), b.getMonth(), b.getDate()),
    ].sort()

    return Math.floor((dates[1] - dates[0]) / _MS_PER_DAY)
}

export const printActivity = (rows: StratzMatch[]) => {
    const oldestMatch = new Date(convertDate(rows[0].startDateTime))
    const x = new Date()
    const now = new Date()

    const rowsByDay: { [key: number]: StratzMatch[] } = rows.reduce(
        (acc, row) => {
            const date = new Date(convertDate(row.startDateTime))
            const dateIndex = setTimeToZero(date).getTime()
            if (acc[dateIndex] == null) {
                acc[dateIndex] = []
            }

            acc[dateIndex].push(row)
            return acc
        },
        {}
    )

    const daysToPrint = dateDiffInDays(oldestMatch, now) + 1
    // [week][day][matches]
    const dateRowByWeek: StratzMatch[][][] = [[]]

    // Sunday - Saturday : 0 - 6
    let prevDay = -1
    let weekIndex = 0
    for (let i = 0; i < daysToPrint; i++) {
        const currentDay = new Date()
        currentDay.setDate(currentDay.getDate() - i)

        // Sunday - Saturday : 0 - 6
        const dayIndex = currentDay.getDay()
        const dateindex = setTimeToZero(currentDay).getTime()
        const isNewWeek = prevDay === 1 && currentDay.getDay() === 0

        if (isNewWeek) {
            dateRowByWeek.push([])
            weekIndex += 1
        }

        if (dateRowByWeek[weekIndex][dayIndex] == null) {
            dateRowByWeek[weekIndex][dayIndex] = []
        }

        if (rowsByDay[dateindex] != null) {
            dateRowByWeek[weekIndex][dayIndex].push(...rowsByDay[dateindex])
        }

        // Sunday - Saturday : 0 - 6
        prevDay = currentDay.getDay()
    }

    console.log(getHeaderString())
    console.log()

    dateRowByWeek.forEach((x) => {
        let matchCountRow: string[] = []
        let winLossRow: string[] = []
        let winPercentRow: string[] = []
        const weeklyGames: StratzMatch[] = []

        DAY_ORDER.forEach((i) => {
            const matches = x[i]
            if (matches == null || matches.length === 0) {
                matchCountRow.push(EMPTY_CELL)
                winLossRow.push(EMPTY_CELL)
                winPercentRow.push(EMPTY_CELL)
                return
            }
            const wr = winrate(
                matches,
                matches.length,
                (x) => getCurrentPlayer(x).isVictory
            )
            const winCount =
                G +
                matches.filter((m) => getCurrentPlayer(m).isVictory).length +
                NC
            const lossCount =
                R +
                matches.filter((m) => !getCurrentPlayer(m).isVictory).length +
                NC

            matchCountRow.push(
                expandWithSpaces(matches.length.toString(), 7, 'CENTER')
            )
            winLossRow.push(
                `${expandWithSpaces(winCount, 2)} - ${expandWithSpaces(
                    lossCount,
                    2
                )}`
            )
            const percentString = percent(wr)
            winPercentRow.push(
                expandWithSpaces(
                    (wr >= 0.5 ? G : R) + percentString + NC,
                    7,
                    'CENTER'
                )
            )

            weeklyGames.push(...matches)
        })

        const weeklyWins = weeklyGames.filter(
            (x) => getCurrentPlayer(x).isVictory
        ).length
        const weeklyLosses = weeklyGames.filter(
            (x) => !getCurrentPlayer(x).isVictory
        ).length
        const weeklyWinrate = winrate(
            weeklyGames,
            weeklyGames.length,
            (x) => getCurrentPlayer(x).isVictory
        )

        const width = 8
        const weeklyGamesString =
            weeklyGames.length > 0
                ? expandWithSpaces(
                      weeklyGames.length.toString(),
                      width,
                      'CENTER'
                  )
                : ''
        const weeklyWinLossString =
            weeklyGames.length > 0
                ? ' ' +
                  expandWithSpaces(
                      G + weeklyWins + NC + ' - ' + R + weeklyLosses + NC,
                      width,
                      'CENTER'
                  )
                : ''
        const weeklyWinrateString =
            weeklyGames.length > 0
                ? ' ' +
                  expandWithSpaces(
                      (weeklyWinrate >= 0.5 ? G : R) +
                          percent(weeklyWinrate) +
                          NC,
                      width,
                      'CENTER'
                  )
                : ''

        console.log(
            `${DAY_SEPARATOR} ` +
                matchCountRow.join(`${DAY_SEPARATOR} `) +
                ` ${DAY_SEPARATOR}${DAY_SEPARATOR}` +
                ` ${weeklyGamesString}`
        )
        console.log(
            `${DAY_SEPARATOR} ` +
                winLossRow.join(`${DAY_SEPARATOR} `) +
                ` ${DAY_SEPARATOR}${DAY_SEPARATOR}` +
                ` ${weeklyWinLossString}`
        )
        console.log(
            `${DAY_SEPARATOR} ` +
                winPercentRow.join(`${DAY_SEPARATOR} `) +
                ` ${DAY_SEPARATOR}${DAY_SEPARATOR}` +
                ` ${weeklyWinrateString}`
        )
        console.log()
    })
}
