import { StratzMatch } from '../types'
import { G, NC } from './colors'
import {
    expandWithSpaces,
    getColoredString,
    getTimeStamp,
} from './utils/print-utils'

export const printLogs = (match: StratzMatch) => {
    console.log(`Chat log for match:`)
    match.players
        .reduce<
            {
                message: string
                time: number
                senderHero: string
                sender: string
            }[]
        >((acc, player) => {
            const msgs = player.stats.allTalks.map((x) => {
                return {
                    ...x,
                    senderHero: player.hero.displayName,
                    sender: player.steamAccount.name,
                }
            })
            return [...acc, ...msgs]
        }, [])
        .sort((a, b) => a.time - b.time)
        .forEach((x) => {
            console.log(
                G,
                expandWithSpaces(getTimeStamp(x.time), 5),
                NC,
                getColoredString(expandWithSpaces(x.senderHero, 15)),
                x.message
            )
        })
}
