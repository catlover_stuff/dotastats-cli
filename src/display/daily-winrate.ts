import {
    PlotConfig,
    plot,
    green as chartColorGreen,
    red as chartColorRed,
} from 'asciichart'

import { DATA_WIDTH_LIMIT, DEFAULT_N_ARG, asciichartConfig } from '../constants'
import { StratzMatch } from '../types'
import { averageimp, winrate } from '../calculation-helpers'
import { expandWithSpaces, joinGraphs, percent } from './utils/print-utils'
import { B, G, NC, R } from './colors'
import { convertDate, getCurrentPlayer } from '../data-interface'

const NUMBER_TO_DAY_OF_WEEK_MAP = [
    [1, 'Monday'],
    [2, 'Tuesday'],
    [3, 'Wednesday'],
    [4, 'Thursday'],
    [5, 'Friday'],
    [6, 'Saturday'],
    [0, 'Sunday'],
] as const

export const printDailyWinrate = (rows: StratzMatch[]) => {
    // 0: Sun, 1: Mon, ...
    const days: { [key: number]: StratzMatch[] } = {
        0: [],
        1: [],
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
    } as const

    rows.forEach((row) => {
        days[new Date(convertDate(row.startDateTime)).getDay()].push(row)
    })

    const res = Object.entries(days).reduce<{
        [key: string]: {
            winrate: number
            wins: number
            losses: number
            averageimp: number
            matchCount: number
        }
    }>((acc, [key, rowsByDay]) => {
        return {
            ...acc,
            [key]: {
                winrate: winrate(
                    rowsByDay,
                    rowsByDay.length,
                    (x) => getCurrentPlayer(x).isVictory
                ),
                wins: rowsByDay.filter((x) => getCurrentPlayer(x).isVictory)
                    .length,
                losses: rowsByDay.filter((x) => !getCurrentPlayer(x).isVictory)
                    .length,
                averageimp: averageimp(rowsByDay),
                matchCount: rowsByDay.length,
            },
        }
    }, {})

    const matchCount = Object.values(res).reduce(
        (acc, x) => acc + (x.matchCount || 0),
        0
    )

    const DAY_WIDTH = 14
    const VAL_WIDTH = 8

    console.log(
        expandWithSpaces(B + 'Day' + NC, DAY_WIDTH),
        expandWithSpaces(B + 'Count' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Win' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Loss' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Win%' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'IMP' + NC, VAL_WIDTH)
    )
    console.log()
    NUMBER_TO_DAY_OF_WEEK_MAP.forEach(([index, dayString]) => {
        const val = res[Number(index)]

        const matchCountString = val.matchCount.toString()
        const winString = G + val.wins + NC
        const lossString = R + val.losses + NC
        const winrateString =
            (val.winrate >= 0.5 ? G : R) + percent(val.winrate) + NC
        const averageimpString =
            (val.averageimp > 0 ? G : R) + val.averageimp.toFixed(1) + NC

        console.log(
            expandWithSpaces(B + dayString + NC, DAY_WIDTH),
            expandWithSpaces(matchCountString, VAL_WIDTH),
            expandWithSpaces(winString, VAL_WIDTH),
            expandWithSpaces(lossString, VAL_WIDTH),
            expandWithSpaces(winrateString, VAL_WIDTH),
            expandWithSpaces(averageimpString, VAL_WIDTH)
        )
    })

    const COL_WIDTH = 5
    console.log()
    console.log(
        `   ${B}Winrate${NC}: Mon  Tue  Wed  Thu  Fri  Sat  Sun` +
            `    ${B}Matches${NC}: Mon  Tue  Wed  Thu  Fri  Sat  Sun` +
            `          ${B}IMP${NC}: Mon  Tue  Wed  Thu  Fri  Sat  Sun`
    )
    console.log(
        joinGraphs(
            plot(
                [
                    [...Array(7 * COL_WIDTH).keys()].map(() => 0.5),
                    NUMBER_TO_DAY_OF_WEEK_MAP.reduce((acc, [index]) => {
                        return [
                            ...acc,
                            ...[...Array(COL_WIDTH).keys()].map(() =>
                                Number.isNaN(res[Number(index)].winrate)
                                    ? 0.5
                                    : res[Number(index)].winrate
                            ),
                        ]
                    }, []),
                ],
                {
                    ...asciichartConfig,
                    format: (x) => expandWithSpaces(percent(x), 9, 'CENTER'),
                    colors: [chartColorRed, chartColorGreen],
                }
            ),
            plot(
                NUMBER_TO_DAY_OF_WEEK_MAP.reduce((acc, [index]) => {
                    return [
                        ...acc,
                        ...[...Array(COL_WIDTH).keys()].map(
                            () => res[Number(index)].matchCount || 0
                        ),
                    ]
                }, []),
                {
                    ...asciichartConfig,
                    format: (x) =>
                        expandWithSpaces(Math.floor(x).toString(), 9, 'CENTER'),
                    colors: [chartColorGreen],
                }
            ),
            plot(
                [
                    [...Array(7 * COL_WIDTH).keys()].map(() => 0),
                    NUMBER_TO_DAY_OF_WEEK_MAP.reduce((acc, [index]) => {
                        return [
                            ...acc,
                            ...[...Array(COL_WIDTH).keys()].map(
                                () => res[Number(index)].averageimp || 0
                            ),
                        ]
                    }, []),
                ],
                {
                    ...asciichartConfig,
                    colors: [chartColorRed, chartColorGreen],
                }
            )
        )
    )
}
