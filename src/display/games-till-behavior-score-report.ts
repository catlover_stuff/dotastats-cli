import { config } from '../constants'
import { StratzMatch } from '../types'

const getMatchesTillBehaviorScoreReport = (rows: StratzMatch[]) => {
    if (config.MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT == null) return -1

    const matchIdIndexWithBehaviorScoreReport = rows.findIndex(
        (x) => x.id === config.MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT
    )

    if (matchIdIndexWithBehaviorScoreReport === -1) return -1
    const val = 15 - ((rows.length - matchIdIndexWithBehaviorScoreReport) % 15)
    return val
}

export const printGamesTillBehaviorScore = (rows: StratzMatch[]) => {
    const x = getMatchesTillBehaviorScoreReport(rows)
    if (x !== -1) {
        console.log('Next behavior score report in: ', x)
    } else {
        console.log(
            'Couldn\'t determine next match with behavior score report. Is "MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT" set?'
        )
    }
}
