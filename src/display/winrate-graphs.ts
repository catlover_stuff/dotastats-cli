import {
    PlotConfig,
    plot,
    green as chartColorGreen,
    blue as chartColorBlue,
} from 'asciichart'

import { DATA_WIDTH_LIMIT, DEFAULT_N_ARG, asciichartConfig } from '../constants'
import { StratzMatch } from '../types'
import { averageimp, winrate } from '../calculation-helpers'
import { FilterConfig, filterData } from '../data-utils'
import { getCurrentPlayer } from '../data-interface'

const printWindowedGraph = (
    rows: StratzMatch[],
    matchCount: number,
    chartDataFn: (window: StratzMatch[], matchCount: number) => number,
    plotConfig?: PlotConfig
) => {
    let window = [...rows]
    const chartData: number[] = []
    while (window.length >= matchCount) {
        chartData.unshift(chartDataFn(window, matchCount))
        window = window.slice(0, window.length - 1)
    }

    const data = chartData.slice(-DATA_WIDTH_LIMIT)

    console.log(
        plot([[...Array(data.length).keys()].map(() => 0.5), data], {
            ...(plotConfig || asciichartConfig),
            colors: [chartColorGreen, chartColorBlue],
        })
    )
}

export const printWinrateGraph = (
    rows: StratzMatch[],
    n: number,
    plotConfig?: PlotConfig
) => {
    const matchCount = n || DEFAULT_N_ARG

    console.log(`Winrate considering past ${matchCount} games:`)
    printWindowedGraph(
        rows,
        matchCount,
        (r, c) => winrate(r, c, (x) => getCurrentPlayer(x).isVictory),
        plotConfig
    )
}

export const printAverageIMPGraph = (rows: StratzMatch[], n: number) => {
    const matchCount = n || DEFAULT_N_ARG

    console.log(`IMP considering past ${matchCount} games:`)
    printWindowedGraph(rows, matchCount, (r, c) => averageimp(r, c))
}
