import { convertDate, getCurrentPlayer } from '../data-interface'
import { StratzMatch } from '../types'
import { getDurationPrettyString, setTimeToZero } from '../utils'
import { B, COLORS, G, NC, R } from './colors'
import { expandWithSpaces } from './utils/print-utils'

export const listMatches = (rows: StratzMatch[]) => {
    console.log(
        expandWithSpaces(G + 'Hero played' + NC, 20),
        expandWithSpaces(G + 'Result' + NC, 7),
        expandWithSpaces(G + 'Date / time' + NC, 30),
        expandWithSpaces(G + 'Duration' + NC, 10),
        expandWithSpaces(G + 'Type' + NC, 10),
        expandWithSpaces(G + 'ID' + NC, 10)
    )
    console.log()

    let prevDayNameMod = 0
    let prevDayTime = 0
    ;[...rows].reverse().forEach((match) => {
        const nameMod = Array.from(
            getCurrentPlayer(match).hero.displayName
        ).reduce((acc, x) => acc + x.charCodeAt(0), 0)
        const nameColor = COLORS[nameMod % COLORS.length]

        const matchDayDate = setTimeToZero(
            new Date(convertDate(match.startDateTime))
        ).getTime()
        if (prevDayTime !== matchDayDate) {
            prevDayNameMod += 1
            prevDayTime = matchDayDate
        }

        const dateColor = COLORS[prevDayNameMod % COLORS.length]

        const durationString = getDurationPrettyString(match.durationSeconds)

        console.log(
            expandWithSpaces(
                `${nameColor}${getCurrentPlayer(match).hero.displayName}${NC}`,
                20
            ),
            expandWithSpaces(
                getCurrentPlayer(match).isVictory
                    ? `${G}Win${NC}`
                    : `${R}Loss${NC}`,
                7
            ),
            expandWithSpaces(
                dateColor +
                    new Date(
                        convertDate(match.startDateTime)
                    ).toLocaleString() +
                    NC,
                30
            ),
            expandWithSpaces(G + durationString + NC, 10),
            expandWithSpaces(B + match.lobbyType + NC, 10),
            expandWithSpaces(G + match.id + NC, 10)
        )
    })
}
