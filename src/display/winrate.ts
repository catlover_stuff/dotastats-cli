import { FilterConfig } from '../data-utils'
import { STATS_KEYS, getStatsFromRows } from '../stratz'
import { StratzMatch } from '../types'
import { B, G, NC, R } from './colors'
import { logJustified, percent } from './utils/print-utils'

export const printWinrate = (rows: StratzMatch[]) => {
    const matchCountToCheckBy = [25, 50, 100, 200, 500, 5000]
    const stats = getStatsFromRows(rows, matchCountToCheckBy)

    const gwr = (wr: number, treshold = 0.5) => {
        const c = wr >= treshold ? G : R
        return `${c}${percent(wr)}${NC}`
    }
    const gimp = (imp: number) => {
        const c = imp >= 0 ? G : R
        return `${c}${imp}${NC}`
    }

    const statsGetters: {
        [K in keyof typeof STATS_KEYS]: (x: number) => string
    } = {
        [STATS_KEYS.winrate]: (x) => gwr(stats[`${STATS_KEYS.winrate}${x}`]),
        [STATS_KEYS.lanewinrate]: (x) =>
            gwr(stats[`${STATS_KEYS.lanewinrate}${x}`]),
        [STATS_KEYS.imp]: (x) => gimp(stats[`${STATS_KEYS.imp}${x}`]),
    }

    const res = [
        [
            `${B}${NC}`,
            `${B}Winrate${NC}`,
            `${B}Lane winrate${NC}`,
            `${B}IMP${NC}`,
        ],
    ]

    matchCountToCheckBy.forEach((x) => {
        res.push([
            `${B}${x}${NC}:`,
            ...Object.values(statsGetters).map((getter) => {
                return getter(x)
            }),
        ])
    })

    res.forEach((x) => {
        logJustified(x)
    })
}
