import { averageimp, winrate } from '../calculation-helpers'
import { DETAILED_BRACKETS, asciichartConfig } from '../constants'
import { getCurrentPlayer, getLaneOutcomes } from '../data-interface'
import { STATS_KEYS, getStatsFromRows } from '../stratz'
import { StratzMatch } from '../types'
import { B, COLORS, G, NC, R } from './colors'
import { expandWithSpaces, logJustified, percent } from './utils/print-utils'
import {
    plot,
    green as chartColorGreen,
    red as chartColorRed,
} from 'asciichart'

const printGraphLabel = () => {
    console.log(
        `             U ${G}H 2 3 4 5 ${B}G 2 3 4 5 ${G}C 2 3 4 5 ${B}A 2 3 4 5 ${G}L 2 3 4 5 ${B}A 2 3 4 5 ${G}D 2 3 4 5 ${B}I${NC}`
    )
}

export const printGamesByRank = (rows: StratzMatch[]) => {
    const rowsByRank = rows.reduce<{ [key: number]: StratzMatch[] }>(
        (acc, row) => {
            // const id = Math.trunc(row.rank / 10)
            const id = row.rank

            return {
                ...acc,
                [id]: [...(acc[id] || []), row],
            }
        },
        {}
    )

    const NAME_WIDTH = 14
    const VAL_WIDTH = 8

    console.log(
        expandWithSpaces('', NAME_WIDTH),
        expandWithSpaces(B + 'Count' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Winrate' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Lane wr' + NC, VAL_WIDTH),
        expandWithSpaces(B + 'Avg imp' + NC, VAL_WIDTH)
    )

    console.log()

    Object.entries(DETAILED_BRACKETS).forEach(([index, bracketName]) => {
        const values = rowsByRank[index] || []

        if (values.length === 0) return

        const nameMod = Array.from(
            bracketName.slice(0, -2) + bracketName[1] + bracketName[4]
        ).reduce((acc, x) => acc + x.charCodeAt(0), 0)

        const c = COLORS[nameMod % COLORS.length]
        const bracketNameString = c + bracketName + NC
        const countString = values.length.toString()
        const wr = winrate(
            values,
            values.length,
            (x) => getCurrentPlayer(x).isVictory
        )
        const winrateString = (wr >= 0.5 ? G : R) + percent(wr) + NC
        const laneWinrate = winrate(
            values,
            values.length,
            (x) => getLaneOutcomes(getCurrentPlayer(x), x).isLaneWin
        )
        const laneWinrateString =
            (laneWinrate >= 0.5 ? G : R) + percent(laneWinrate) + NC
        const imp = averageimp(values)
        const impString = (imp >= 0 ? G : R) + imp.toFixed(1) + NC

        console.log(
            expandWithSpaces(bracketNameString, NAME_WIDTH),
            expandWithSpaces(countString, VAL_WIDTH),
            expandWithSpaces(winrateString, VAL_WIDTH),
            expandWithSpaces(laneWinrateString, VAL_WIDTH),
            expandWithSpaces(impString, VAL_WIDTH)
        )
    })

    const COL_WIDTH = 2
    console.log()
    console.log('Game count')
    console.log(
        plot(
            [
                Object.entries(DETAILED_BRACKETS).reduce((acc, [index]) => {
                    const values = rowsByRank[index] || []
                    return [
                        ...acc,
                        ...[...Array(COL_WIDTH).keys()].map(
                            () => values.length || 0
                        ),
                    ]
                }, []),
            ],
            {
                ...asciichartConfig,
                height: 7,
                colors: [chartColorGreen],
            }
        )
    )
    printGraphLabel()
    console.log()
    console.log('Winrate')
    console.log(
        plot(
            [
                [
                    ...Array(
                        Object.entries(DETAILED_BRACKETS).length * COL_WIDTH
                    ).keys(),
                ].map(() => 0.5),
                Object.entries(DETAILED_BRACKETS).reduce((acc, [index]) => {
                    const values = rowsByRank[index] || []
                    const wr = winrate(
                        values,
                        values.length,
                        (x) => getCurrentPlayer(x).isVictory
                    )
                    return [
                        ...acc,
                        ...[...Array(COL_WIDTH).keys()].map(() => wr || 0),
                    ]
                }, []),
            ],
            {
                ...asciichartConfig,
                height: 7,
                colors: [chartColorRed, chartColorGreen],
            }
        )
    )
    printGraphLabel()
    console.log()
    console.log('IMP')
    console.log(
        plot(
            [
                [
                    ...Array(
                        Object.entries(DETAILED_BRACKETS).length * COL_WIDTH
                    ).keys(),
                ].map(() => 0),
                Object.entries(DETAILED_BRACKETS).reduce((acc, [index]) => {
                    const values = rowsByRank[index] || []
                    const avgimp = averageimp(values)
                    return [
                        ...acc,
                        ...[...Array(COL_WIDTH).keys()].map(() => avgimp || 0),
                    ]
                }, []),
            ],
            {
                ...asciichartConfig,
                height: 7,
                colors: [chartColorRed, chartColorGreen],
            }
        )
    )
    printGraphLabel()
}
