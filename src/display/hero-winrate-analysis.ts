import { convertDate, getCurrentPlayer } from '../data-interface'
import { StratzMatch } from '../types'
import { COLORS, G, NC, R } from './colors'
import { expandWithSpaces, percent } from './utils/print-utils'

const w = (x) => process.stdout.write(x)

const intervals = [50, 100, 200, 500, 5000] as const
const minMatchLimits = [2, 2, 3, 4, 10] as const

const columnWidth = 31 as const

const getHeroWinrateAnalysis = (rows: StratzMatch[]) => {
    const result = intervals.map((interval, intervalIndex) => {
        const currentWindow = rows.slice(-interval)

        const endDate = new Date(convertDate(currentWindow[0].startDateTime))
        const startDate = new Date(
            convertDate(currentWindow.at(-1).startDateTime)
        )

        const heros = currentWindow.reduce<{
            [heroName: string]: {
                wins: number
                losses: number
            }
        }>((acc, row) => {
            const {
                hero: { displayName },
                isVictory,
            } = getCurrentPlayer(row)

            return {
                ...acc,
                [displayName]: {
                    wins: isVictory
                        ? (acc[displayName]?.wins || 0) + 1
                        : acc[displayName]?.wins || 0,
                    losses: !isVictory
                        ? (acc[displayName]?.losses || 0) + 1
                        : acc[displayName]?.losses || 0,
                },
            }
        }, {})

        const herosWithWinrates = Object.entries(heros)
            .reduce<
                {
                    wins: number
                    losses: number
                    name: string
                    winrate: number
                }[]
            >((acc, [key, value]) => {
                return [
                    ...acc,
                    {
                        ...value,
                        name: key,
                        winrate: value.wins / (value.losses + value.wins),
                    },
                ]
            }, [])
            .sort((a, b) => b.winrate - a.winrate)
            .filter((hero) => {
                return (
                    hero.wins + hero.losses >=
                    (minMatchLimits[intervalIndex] || 0)
                )
            })

        return { herosWithWinrates, startDate, endDate }
    })

    return result
}

export const printHeroWinrateAnalysis = (rows: StratzMatch[]) => {
    const heroWinrateAnalytics = getHeroWinrateAnalysis(rows)

    const maxRows = Math.max(
        ...heroWinrateAnalytics.map((x) => x.herosWithWinrates.length)
    )

    for (let colIndex = 0; colIndex < intervals.length; colIndex++) {
        w(
            expandWithSpaces(
                `In ${G}${intervals[colIndex].toString()}${NC} matches (min: ${
                    minMatchLimits[colIndex]
                })`,
                columnWidth
            )
        )
    }

    w('\n')
    for (let colIndex = 0; colIndex < intervals.length; colIndex++) {
        const { startDate, endDate } = heroWinrateAnalytics[colIndex]

        w(
            expandWithSpaces(
                `${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`,
                columnWidth
            )
        )
    }
    w('\n')
    w('\n')

    for (let rowIndex = 0; rowIndex < maxRows; rowIndex++) {
        for (let colIndex = 0; colIndex < intervals.length; colIndex++) {
            const { herosWithWinrates } = heroWinrateAnalytics[colIndex]
            const hero = herosWithWinrates[rowIndex]

            if (hero) {
                const nameMod = Array.from(hero.name).reduce(
                    (acc, x) => acc + x.charCodeAt(0),
                    0
                )
                const c = COLORS[nameMod % COLORS.length]

                const heroString = `${c}${expandWithSpaces(hero.name, 12)}${NC}`
                const matchCountString = expandWithSpaces(
                    (hero.wins + hero.losses).toString(),
                    3
                )
                // FIXME: players with a lot of matches have these cut off
                const winString = `${G}${expandWithSpaces(
                    hero.wins.toString(),
                    3
                )}${NC}`
                const lossString = `${R}${expandWithSpaces(
                    hero.losses.toString(),
                    3
                )}${NC}`
                const winrateString = `${
                    hero.winrate >= 0.5 ? G : R
                }${expandWithSpaces(percent(hero.winrate), 5)}${NC}`

                w(
                    expandWithSpaces(
                        heroString +
                            ' ' +
                            matchCountString +
                            ' ' +
                            winString +
                            '- ' +
                            lossString +
                            winrateString +
                            ' ',
                        columnWidth
                    )
                )
            } else {
                w(expandWithSpaces('', columnWidth))
            }
        }
        w('\n')
    }
}
