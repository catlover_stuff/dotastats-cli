import { GameplayStats, POSITIONS, Position, StratzMatch } from '../types'

import { B, C, COLORS, G, Grey, M, NC, R, Y } from './colors'
import { DETAILED_BRACKETS, asciichartConfig, config } from '../constants'
import { FilterConfig, filterData } from '../data-utils'
import { STATS_KEYS, getStatsFromRows } from '../stratz'
import {
    expandWithSpaces,
    getColoredString,
    logJustified,
    percent,
} from './utils/print-utils'
import { getDurationPrettyString } from '../utils'
import {
    convertDate,
    getCurrentPlayer,
    getLaneOutcomes,
    getSide,
    isParsed,
} from '../data-interface'

const LABEL_WIDTH = 12

const POSITION_TO_STRING_MAP: { [key in Position]: string } = {
    ALL: 'All',
    POSITION_1: '1',
    POSITION_2: '2',
    POSITION_3: '3',
    POSITION_4: '4',
    POSITION_5: '5',
    UNKNOWN: 'Unknown',
    FILTERED: 'Filtered',
}

export const ANALYSIS_ROW_TYPES = {
    RANK: 'RANK',
    SIDE: 'SIDE',
    GAME_ID: 'GAME_ID',
    DATE: 'DATE',
    DURATION: 'DURATION',
    OUTCOME: 'OUTCOME',
    MY_LANE_OUTCOME: 'MY_LANE_OUTCOME',
    OTHER_SIDE_LANE_OUTCOME: 'OTHER_SIDE_LANE_OUTCOME',
    TOP_LANE_OUTCOME: 'TOP_LANE_OUTCOME',
    MID_LANE_OUTCOME: 'MID_LANE_OUTCOME',
    BOTTOM_LANE_OUTCOME: 'BOTTOM_LANE_OUTCOME',
    POSITION: 'POSITION',
    HERO: 'HERO',
    PARTY: 'PARTY',
    AWARD: 'AWARD',
    IMP: 'IMP',
    LOBBY: 'LOBBY',
    GAME_MODE: 'GAME_MODE',
} as const
export type AnalysisRowTypes = keyof typeof ANALYSIS_ROW_TYPES

const DEFAULT_CONFIG_ALANYSIS_ROWS: AnalysisRowTypes[] = [
    ANALYSIS_ROW_TYPES.OUTCOME,
    ANALYSIS_ROW_TYPES.MY_LANE_OUTCOME,
    ANALYSIS_ROW_TYPES.SIDE,
    ANALYSIS_ROW_TYPES.TOP_LANE_OUTCOME,
    ANALYSIS_ROW_TYPES.MID_LANE_OUTCOME,
    ANALYSIS_ROW_TYPES.BOTTOM_LANE_OUTCOME,
    ANALYSIS_ROW_TYPES.POSITION,
    ANALYSIS_ROW_TYPES.DURATION,
    ANALYSIS_ROW_TYPES.HERO,
    ANALYSIS_ROW_TYPES.PARTY,
    ANALYSIS_ROW_TYPES.AWARD,
    ANALYSIS_ROW_TYPES.IMP,
    ANALYSIS_ROW_TYPES.DATE,
    ANALYSIS_ROW_TYPES.GAME_ID,
]

const ANALYSIS_COLUMN_HEADERS: { [key in AnalysisRowTypes]: string } = {
    [ANALYSIS_ROW_TYPES.RANK]: 'Rank',
    [ANALYSIS_ROW_TYPES.SIDE]: 'Side',
    [ANALYSIS_ROW_TYPES.GAME_ID]: 'Game ID',
    [ANALYSIS_ROW_TYPES.DATE]: 'Date',
    [ANALYSIS_ROW_TYPES.DURATION]: 'Duration',
    [ANALYSIS_ROW_TYPES.OUTCOME]: 'Outcome',
    [ANALYSIS_ROW_TYPES.MY_LANE_OUTCOME]: 'Lane',
    [ANALYSIS_ROW_TYPES.OTHER_SIDE_LANE_OUTCOME]: 'Other',
    [ANALYSIS_ROW_TYPES.TOP_LANE_OUTCOME]: 'Top',
    [ANALYSIS_ROW_TYPES.MID_LANE_OUTCOME]: 'Mid',
    [ANALYSIS_ROW_TYPES.BOTTOM_LANE_OUTCOME]: 'Bot',
    [ANALYSIS_ROW_TYPES.POSITION]: 'Position',
    [ANALYSIS_ROW_TYPES.HERO]: 'Hero',
    [ANALYSIS_ROW_TYPES.PARTY]: 'Party',
    [ANALYSIS_ROW_TYPES.AWARD]: 'Award',
    [ANALYSIS_ROW_TYPES.IMP]: 'IMP',
    [ANALYSIS_ROW_TYPES.LOBBY]: 'Lobby type',
    [ANALYSIS_ROW_TYPES.GAME_MODE]: 'Game mode',
}

const SMALL_COLUMN_WIDTH = 4
const ANALYSIS_COLUMN_WIDTHS: { [key in AnalysisRowTypes]: number } = {
    [ANALYSIS_ROW_TYPES.RANK]: 10,
    [ANALYSIS_ROW_TYPES.SIDE]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.GAME_ID]: 10,
    [ANALYSIS_ROW_TYPES.DATE]: 20,
    [ANALYSIS_ROW_TYPES.DURATION]: 8,
    [ANALYSIS_ROW_TYPES.OUTCOME]: 6,
    [ANALYSIS_ROW_TYPES.MY_LANE_OUTCOME]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.OTHER_SIDE_LANE_OUTCOME]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.TOP_LANE_OUTCOME]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.MID_LANE_OUTCOME]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.BOTTOM_LANE_OUTCOME]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.POSITION]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.HERO]: 18,
    [ANALYSIS_ROW_TYPES.PARTY]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.AWARD]: SMALL_COLUMN_WIDTH,
    [ANALYSIS_ROW_TYPES.IMP]: 24,
    [ANALYSIS_ROW_TYPES.LOBBY]: 7,
    [ANALYSIS_ROW_TYPES.GAME_MODE]: 9,
}

const w = (x: string) => process.stdout.write(x)

const analysisCellPrinters: {
    [T in AnalysisRowTypes]: (row: StratzMatch) => void
} = {
    [ANALYSIS_ROW_TYPES.GAME_ID]: (row) => {
        w(
            expandWithSpaces(
                row.id.toString(),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.GAME_ID]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.SIDE]: (row) => {
        const side = getSide(getCurrentPlayer(row)) || ''
        w(
            expandWithSpaces(
                (side === 'Radiant' ? G : R) + side + NC,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.SIDE]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.DATE]: (row) => {
        w(
            expandWithSpaces(
                getColoredString(
                    new Date(convertDate(row.startDateTime)).toLocaleString(),
                    new Date(
                        convertDate(row.startDateTime)
                    ).toLocaleDateString()
                ),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.DATE]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.DURATION]: (row) => {
        w(
            expandWithSpaces(
                G + getDurationPrettyString(row.durationSeconds) + NC,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.DURATION]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.RANK]: (row) => {
        w(
            expandWithSpaces(
                getColoredString(
                    DETAILED_BRACKETS[row.rank] || row.rank.toString()
                ),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.RANK]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.OUTCOME]: function (row) {
        let outcomeString = ''
        if (getCurrentPlayer(row).isVictory) {
            outcomeString =
                row.analysisOutcome === 'STOMPED'
                    ? `${G}Stomp${NC}`
                    : `${G}Win${NC}`
        } else {
            outcomeString =
                row.analysisOutcome === 'STOMPED'
                    ? `${R}Stomp${NC}`
                    : `${R}Loss${NC}`
        }
        w(
            expandWithSpaces(
                outcomeString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.OUTCOME]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.MY_LANE_OUTCOME]: function (row) {
        const laneOutcomeString = getLaneDataStringToPrint(row, 'myLaneOutcome')
        w(
            expandWithSpaces(
                laneOutcomeString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.MY_LANE_OUTCOME]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.OTHER_SIDE_LANE_OUTCOME]: function (row) {
        const otherLaneOutcomeString = getLaneDataStringToPrint(
            row,
            'otherSideLaneOutcome'
        )
        w(
            expandWithSpaces(
                otherLaneOutcomeString,
                ANALYSIS_COLUMN_WIDTHS[
                    ANALYSIS_ROW_TYPES.OTHER_SIDE_LANE_OUTCOME
                ]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.TOP_LANE_OUTCOME]: function (row) {
        const topLaneOutcomeString = getLaneDataStringToPrint(
            row,
            'topLaneOutcome'
        )
        w(
            expandWithSpaces(
                topLaneOutcomeString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.TOP_LANE_OUTCOME]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.MID_LANE_OUTCOME]: function (row) {
        const midLaneOutcomeString = getLaneDataStringToPrint(
            row,
            'midLaneOutcome'
        )
        w(
            expandWithSpaces(
                midLaneOutcomeString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.MID_LANE_OUTCOME]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.BOTTOM_LANE_OUTCOME]: function (row) {
        const bottomLaneOutcomeString = getLaneDataStringToPrint(
            row,
            'bottomLaneOutcome'
        )
        w(
            expandWithSpaces(
                bottomLaneOutcomeString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.BOTTOM_LANE_OUTCOME]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.HERO]: function (row) {
        w(
            expandWithSpaces(
                getColoredString(getCurrentPlayer(row).hero.displayName),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.HERO]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.PARTY]: function (row) {
        const partyString =
            getCurrentPlayer(row).partyId != null ? `${C}P${NC}` : ' '
        w(
            expandWithSpaces(
                partyString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.PARTY]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.AWARD]: function (row) {
        let awardString = ''
        if (getCurrentPlayer(row).award === 'MVP') {
            awardString = `${B}M${NC}`
        } else if (
            getCurrentPlayer(row).award === 'TOP_SUPPORT' ||
            getCurrentPlayer(row).award === 'TOP_CORE'
        ) {
            awardString = `${C}T${NC}`
        } else {
            awardString = ' '
        }
        w(
            expandWithSpaces(
                awardString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.AWARD]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.IMP]: function (row) {
        w(
            expandWithSpaces(
                getImpStatString(row),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.IMP]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.LOBBY]: function (row) {
        const lobbyString = getColoredString(row.lobbyType)
        w(
            expandWithSpaces(
                getColoredString(lobbyString),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.LOBBY]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.GAME_MODE]: function (row) {
        const gameModeString = getColoredString(row.gameMode)
        w(
            expandWithSpaces(
                getColoredString(gameModeString),
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.GAME_MODE]
            )
        )
    },
    [ANALYSIS_ROW_TYPES.POSITION]: function (row) {
        const positionString = getColoredString(
            POSITION_TO_STRING_MAP[getCurrentPlayer(row).position]
        )
        w(
            expandWithSpaces(
                positionString,
                ANALYSIS_COLUMN_WIDTHS[ANALYSIS_ROW_TYPES.POSITION]
            )
        )
    },
}

const getLaneDataStringToPrint = (
    row: StratzMatch,
    accessor:
        | 'myLaneOutcome'
        | 'midLaneOutcome'
        | 'topLaneOutcome'
        | 'bottomLaneOutcome'
        | 'otherSideLaneOutcome'
) => {
    const {
        isLaneWin: _isLaneWin,
        isLaneLoss: _isLaneLoss,
        ...restOutcomes
    } = getLaneOutcomes(getCurrentPlayer(row), row)

    if (!isParsed(row)) return ''

    if (restOutcomes[accessor] === 'WIN') {
        return `${G}W${NC}`
    } else if (restOutcomes[accessor] === 'LOSS') {
        return `${R}L${NC}`
    } else if (restOutcomes[accessor] === 'LOSS_STOMP') {
        return `${R}S${NC}`
    } else if (restOutcomes[accessor] === 'WIN_STOMP') {
        return `${Y}S${NC}`
    } else {
        return `${Grey}T${NC}`
    }
}

const IMP_STRING_LENGTH = 10 // from - to +
// █ ▌ ▐
const getImpStatString = (row: StratzMatch) => {
    const imp = getCurrentPlayer(row)?.imp || 0
    let resultString = expandWithSpaces(`${imp} `, 4)

    const fivesInImp = Math.trunc(imp / 5)
    const rest = imp - fivesInImp * 5

    const resultCharArray = []
    for (let i = 0; i < Math.abs(fivesInImp); i++) {
        resultCharArray.push('█')
    }
    if (Math.abs(rest) >= 3) {
        resultCharArray.push(imp < 0 ? '▐' : '▌')
    }

    if (imp >= 0) {
        resultString += expandWithSpaces('', IMP_STRING_LENGTH)
        resultString += expandWithSpaces(
            resultCharArray.join(''),
            IMP_STRING_LENGTH
        )
    } else {
        ;(resultString += expandWithSpaces(
            [...resultCharArray].join(''),
            IMP_STRING_LENGTH
        )
            .split('')
            .reverse()
            .join('')),
            (resultString += expandWithSpaces('', IMP_STRING_LENGTH))
    }

    return (imp >= 0 ? G : R) + resultString + NC
}

export const printStats = (
    rows: StratzMatch[],
    mainAnalysisRows?: AnalysisRowTypes[]
) => {
    const nonParsedMatchCount = rows.reduce(
        (acc, x) => acc + (isParsed(x) ? 0 : 1),
        0
    )

    if (nonParsedMatchCount > 0) {
        console.log(
            `${R}Data displayed has ${B}${nonParsedMatchCount}${R} matches which are not parsed!${NC}`
        )
    }

    const printConfigToUse =
        mainAnalysisRows && mainAnalysisRows.length > 0
            ? mainAnalysisRows
            : DEFAULT_CONFIG_ALANYSIS_ROWS

    console.log()
    printConfigToUse.forEach((x) => {
        w(
            G +
                expandWithSpaces(
                    ANALYSIS_COLUMN_HEADERS[x],
                    ANALYSIS_COLUMN_WIDTHS[x]
                ) +
                NC
        )
        w(' ')
    })
    w('\n')
    w('\n')
    ;[...rows].reverse().forEach((row) => {
        printConfigToUse.forEach((x) => {
            const printer = analysisCellPrinters[x]
            if (printer) {
                printer(row)
                w(' ')
            } else {
                console.log(`${R}Error: ${x} is not a valid analysis row type!`)
            }
        })
        w('\n')
    })
}
