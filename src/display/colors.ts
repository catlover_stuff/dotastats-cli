export const R = '\x1b[31m'
export const G = '\x1b[32m'
export const Y = '\x1b[33m'
export const B = '\x1b[34m'
export const M = '\x1b[35m'
export const C = '\x1b[36m'

export const BG = '\x1b[42m'

export const Grey = '\x1b[90m'
export const Black = '\x1b[30m'

export const NC = '\x1b[0m'

export const COLORS = [R, G, B, C, M, Y]

export const ALL_COLOR_CODES = [...COLORS, Grey, NC]

// const Reset = "\x1b[0m"
// const Bright = "\x1b[1m"
// const Dim = "\x1b[2m"
// const Underscore = "\x1b[4m"
// const Blink = "\x1b[5m"
// const Reverse = "\x1b[7m"
// const Hidden = "\x1b[8m"
//
// const FgRed = "\x1b[31m"
// const FgGreen = "\x1b[32m"
// const FgYellow = "\x1b[33m"
// const FgBlue = "\x1b[34m"
// const FgMagenta = "\x1b[35m"
// const FgCyan = "\x1b[36m"
// const FgWhite = "\x1b[37m"
// const FgGray = "\x1b[90m"
//
// const BgBlack = "\x1b[40m"
// const BgRed = "\x1b[41m"
// const BgGreen = "\x1b[42m"
// const BgYellow = "\x1b[43m"
// const BgBlue = "\x1b[44m"
// const BgMagenta = "\x1b[45m"
// const BgCyan = "\x1b[46m"
// const BgWhite = "\x1b[47m"
// const BgGray = "\x1b[100m"
