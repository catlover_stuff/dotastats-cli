import { StratzMatch } from '../types'
import { G, NC } from './colors'

const getTotalTimeFromMatches = (rows: StratzMatch[]) => {
    const total = rows.reduce(
        (acc, x) =>
            acc + (Number.isNaN(+x.durationSeconds) ? 0 : x.durationSeconds),
        0
    )

    const hours = Math.floor(total / (60 * 60))
    const minutes = Math.floor((total - hours * (60 * 60)) / 60)
    const seconds = total - hours * (60 * 60) - minutes * 60
    return { hours, minutes, seconds }
}

export const printTotalGameTime = (rows: StratzMatch[]) => {
    const { hours, minutes, seconds } = getTotalTimeFromMatches(rows)

    console.log(
        `Total game time: ${G}${hours}${NC}:${G}${
            minutes < 10 ? '0' : ''
        }${minutes}${NC}:${G}${seconds < 10 ? '0' : ''}${seconds}${NC}`
    )
}
