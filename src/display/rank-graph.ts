import { plot, green as chartColorGreen } from 'asciichart'
import { asciichartConfig } from '../constants'
import { StratzMatch } from '../types'

export const printRankGraph = (rows: StratzMatch[]) => {
    const chartData = rows.map((x) => {
        return Math.floor(x.rank / 10) * 10 + (x.rank % 10) * 2
    })

    console.log(`Rank graph:`)
    console.log(
        plot(chartData, { ...asciichartConfig, colors: [chartColorGreen] })
    )
}
