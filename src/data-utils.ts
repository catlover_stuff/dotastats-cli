import { convertDate, getCurrentPlayer } from './data-interface'
import {
    GameMode,
    LobbyType,
    PARTY_STATUS,
    StratzMatch,
    PartyStatus,
    Position,
} from './types'

export type FilterConfig = {
    n?: number
    d?: number
    gameMode?: GameMode[]
    lobbyType?: LobbyType[]
    position?: Position[]
    hero?: string[]
    partyStatus?: PartyStatus
}

const filterMultiple = <T>(
    rows: StratzMatch[],
    criteria: T[],
    accessor: (x: StratzMatch) => T
) =>
    rows.filter((row) => {
        return criteria.some((c) => {
            return accessor(row) === c
        })
    })

export const applyN = (rows: StratzMatch[], n: number) => {
    return !Number.isNaN(+n) ? rows.slice(-n) : rows
}

export const applyNAndD = (rows: StratzMatch[], n: number, d: number) => {
    return applyN(applyD(rows, d), n)
}

export const applyD = (rows: StratzMatch[], d: number) => {
    if (!Number.isNaN(+d)) {
        const dateFrom = new Date()
        dateFrom.setDate(dateFrom.getDate() - d || 7)
        dateFrom.setMilliseconds(0)
        dateFrom.setSeconds(0)
        dateFrom.setMinutes(0)
        dateFrom.setHours(0)

        const newRows: StratzMatch[] = []

        for (let i = rows.length - 1; i >= 0; i--) {
            const x = new Date(convertDate(rows[i].startDateTime))
            x.setMilliseconds(0)
            x.setSeconds(0)
            x.setMinutes(0)
            x.setHours(0)
            if (newRows.length > 0 && x < dateFrom) {
                break
            }
            newRows.unshift(rows[i])
        }
        return newRows
    }
    return rows
}

export const filterData = (
    allrows: StratzMatch[],
    filterConfig: Omit<FilterConfig, 'n' | 'd'>
) => {
    const { gameMode, lobbyType, position, hero, partyStatus } =
        filterConfig || {}
    let rows = allrows

    if (partyStatus != null && partyStatus != PARTY_STATUS.BOOTH)
        rows = rows.filter((x) => {
            if (partyStatus === PARTY_STATUS.YES) {
                return getCurrentPlayer(x).partyId != null
            } else if (partyStatus === PARTY_STATUS.NO) {
                return getCurrentPlayer(x).partyId == null
            }
        })
    if (gameMode?.length && !gameMode.includes('ALL'))
        rows = filterMultiple(rows, gameMode, (x) => x.gameMode)
    if (lobbyType?.length && !lobbyType.includes('ALL'))
        rows = filterMultiple(rows, lobbyType, (x) => x.lobbyType)
    if (position?.length && !position.includes('ALL'))
        rows = filterMultiple(
            rows,
            position,
            (x) => getCurrentPlayer(x).position
        )
    if (hero?.length && !hero.includes('ALL'))
        rows = rows.filter((row) => {
            return hero.some((hero) => {
                return getCurrentPlayer(row)
                    .hero.displayName.toLowerCase()
                    .includes(hero.toLowerCase())
            })
        })

    return rows
}
