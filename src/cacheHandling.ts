import fs from 'node:fs'
import { CacheFile, StratzMatch } from './types'
import path from 'node:path'

const cacheDir = 'cache'

if (!fs.existsSync(cacheDir)) {
    fs.mkdirSync(cacheDir)
}

const getDataFile = (steamAccountId: number) => {
    return path.join(cacheDir, `${steamAccountId}.json`)
}

export const saveCache = (data: CacheFile, steamAccountId: number) => {
    // TODO: store cache parsed
    fs.writeFileSync(getDataFile(steamAccountId), JSON.stringify(data))
}

export const getDataFromFile = (steamAccountId: number): CacheFile => {
    try {
        return JSON.parse(fs.readFileSync(getDataFile(steamAccountId), 'utf8'))
    } catch (e) {
        console.log(`Unable to read '${getDataFile(steamAccountId)}'`)
        console.log(e)
    }
}

export const getData = (steamAccountId: number) => {
    if (fs.existsSync(getDataFile(steamAccountId))) {
        return getDataFromFile(steamAccountId)
    } else {
        console.log(`No cache file! (${getDataFile(steamAccountId)})`)
        return null
    }
}
