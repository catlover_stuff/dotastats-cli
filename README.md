# Dota cli stats

Displaying various statistics about games using the stratz API

## Setup

**Requirements**: `node`

 * First run `npm run setup`
 * Create `config.json` in the project root directory (next to 'dotastats' exacutable) with the following content:
    ```json
    {
        "STEAM_ACCOUNT_ID": 123,
        "STRATZ_API_KEY": "asd",
        "MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT": 123,
        "MAIN_ALANYSIS_ROWS": [],
        "MAIN_ALANYSIS_ROWS_WHEN_VIEWING_OTHER": []
    }
    ```
     * **STEAM_ACCOUNT_ID** (*required*): Your steam id (number)
     * **STRATZ_API_KEY** (*required*): Your stratz.com api key. Get one from 'https://stratz.com/api'
         after logging in (string)
     * **MATCH_ID_WITH_BEHAVIOR_SCORE_REPORT** (*optional*): A match after you got a behavior score report.
         Used to calculate when you will get the next periodic one (number)
     * **MAIN_ALANYSIS_ROWS** (*optional*): Which rows to display on the default analysis page. See
         `./dotastats --main-analysis-rows help`
     * **MAIN_ALANYSIS_ROWS_WHEN_VIEWING_OTHER** (*optional*): Same as above, but only used if checking out other
         players stats with `--steam-account-id`


Minimal config:

```json
{ "STEAM_ACCOUNT_ID": 123, "STRATZ_API_KEY": "asd" }
```

## Usage

 * Run `dotastats` (ex.: `./dotastats`)
 * To fetch new matches use `./dotastats fetch-new`
 * To re-fetch a single unparsed match use `./dotastats refetch <matchid>`
 * To re-fetch unparsed matches use `./dotastats refetch-unparsed`
 * To (re) fetch all your data use `./dotastats refetch-all`
    * specify `-n x` to restrict to the last x matches. Defaults to 500
 * For usage check `--help`
 * Most of the analysises accept filters. See `--help` for them, under "Options"
 * For options with choices for ex.: `--game-mode` try `./dotastats --game-mode help` to see choices

### Updating

Sometimes when new type of views are added in newer versions of the script its is advised to
run `./dotastats refetch-all` in order to re-fetch all the data, in case the new version of
the script uses fields that the previous ones did not fetch.

## Bugs

 * Main analysis column type `MY_LANE_OUTCOME` and `OTHER_LANE_OUTCOME` doesn't work correctly, if your
     lane was mid
 * Graphs could have fractional and a bit inaccurate X values, this is because the third party graph
     library the script uses

For feature requests and bug reports please open an issue on gitlab.

## Filters

You can use filters on any of the printed data, for example:
```
./dotastats heros --position POSITION_4 -n 10
Showing data from the last 10 matches
Hero name             Matches       Wins          losses        Win %         Average IMP   Lane win %    Lane loss %

Venomancer            1             0             1             0%            29            0%            0%
Hoodwink              2             0             2             0%            5             0%            100%
Nature's Prophet      2             1             1             50%           15.5          50%           50%
Dark Willow           5             3             2             60%           5.8           20%           20%
```

Available filters can be displayed using the `--help` flag. For a specific filter keys can be viewed
by `./dotastats --filtername help` (`./dotastats --position help`)

If you don't use day or number filters, by default 25 will be used for the number filter.

### Data / cache management

The data is saved in `./cache/${steamAccountId}.json`. By default if its not available for the
default or provided (with `--steam-account-id`) id, the script fetches them.

Management commands (./dotastats <command>):
 * `refetch-all`: refetches all matches from stratz, overwriting existing cache. `-n` specifies how
    many it should fetch.
 * `refetch <matchid>`: refetches specidied matchid, and updates its value in the cache
* `refetch-unparsed`: finds unparsed matches in the cache, and refetches them. `-n` specifies how
    many of the last games it should check
* `fetch-new`: fetches new games since the last one that is available in the cache
* `add-matches-from-file`: Adds matches from file. File should be specified with "--file-name", and
    should contain one match ID per row
* `request-parse <matchid>`: requests stratz to parse the specified `matchid`
* `request-parse-recents`: finds unparsed matches in the cache, and requests stratz to parse each of
    them
* `check-replay-available <matchId>`: checks if `matchid` has a replay available using opendota-s
    api
* `print-not-parsed-matches`: prints the last few not parsed matches. `-n` specifies how many of the
    last matches it should check

## License

GPL v3.0
